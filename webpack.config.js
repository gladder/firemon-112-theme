let path = require('path');
let webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = [
    {
        entry: './assets/src/service-worker.js',
        mode: 'production',
        output: {
            path: path.resolve(__dirname, './assets/'),
            filename: 'sw.js',
        },
    },
    {
        entry: './assets/src/monitor.js',
        module: {
            rules: [
                {
                    test: /\.(js)$/,
                    exclude: /node_modules/,
                    use: ['babel-loader']
                },
                {
                    test: /\.font\.js/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {
                                publicPath: './',
                            }
                        },
                        {
                            loader: 'css-loader',
                            options: {
                                url: false,
                            }
                        }
                    ]
                },
            ]
        },
        resolve: {
            extensions: ['*', '.js']
        },
        mode: 'production',
        output: {
            path: path.resolve(__dirname, './assets/javascript/'),
            filename: 'monitor.js',
        },
        plugins: [
            new webpack.ProvidePlugin({
                //jQuery: require.resolve('jquery'),
                paho: require.resolve('paho-mqtt'),
                io: require.resolve('socket.io-client')
            }),
            new webpack.SourceMapDevToolPlugin({
                filename: '[file].map[query]',
            }),
            new BundleAnalyzerPlugin({
                analyzerMode: 'static',
                openAnalyzer: true
            }),
        ]
    },

];