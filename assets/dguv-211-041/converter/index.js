
const fs = require('fs')
dirTree = require("directory-tree");

const tree = dirTree('./in/');
const { optimize } = require('svgo');
const { convert } = require('convert-svg-to-png');
/*
tree.children.forEach(element => {

    let flattendSvgPath = './out/' + element.name;

    // read SVG file into string
    let svgString = fs.readFileSync('./in/' + element.name, 'utf8')
    console.log(svgString);
    // convert all objects to paths
    let modifiedSvgString = svgFlatten(svgString).pathify().transform('rotate(90)').value()
    console.log(modifiedSvgString);
    // write SVG string to disk
    fs.writeFileSync(flattendSvgPath, modifiedSvgString)

    console.log(element.name + " done");
    exit;

});
*/


const { createConverter } = require('convert-svg-to-png');
const util = require('util');
const { exit } = require('process');

const readdir = util.promisify(fs.readdir);
const sizes = [
    { width: 24, height: 24 },
    { width: 48, height: 48 },
    { width: 96, height: 96 },
    { width: 256, height: 256 },
];

async function convertSvgFiles(dirPath) {
    console.log(dirPath);
    const converter = createConverter();

    try {
        const filePaths = await readdir(dirPath);

        for (const filePath of filePaths) {
            const outputFolder = "./out/";
            for (const size of sizes)  {
                const sizeFolder = size.width+"x"+size.height;
                console.log(filePath + " for " + sizeFolder);
                const outputFilePath = await converter.convertFile(dirPath + filePath,{ width: size.width, height: size.height});
                const fileName = outputFilePath.replace("in", "");
                if (!fs.existsSync(outputFolder + sizeFolder)) {
                    fs.mkdirSync(outputFolder + sizeFolder);
                }

                fs.rename(outputFilePath, './' + outputFolder + sizeFolder + "/" + fileName, function (err) {
                    if (err) console.log('ERROR: ' + err);
                });

                exit;
            }
        }
    } finally {
        await converter.destroy();
    }
}

convertSvgFiles("./in/");


function foo() {
    tree.children.forEach(element => {

        let svg_in = './in/' + element.name;
        let svg_out_svg = './out/' + element.name ;
        let svg_out_png = './out/' + element.name.replace("svg", "png");

        fs.readFile(svg_in, async(err, input) => {
            console.log("Read file " + svg_in);
            /*const result = optimize(input, {
                // optional but recommended field
                path: svg_in,
                // all config fields are also available here
                multipass: true,
            });*/

            //fs.writeFileSync(svg_out_svg, result.data);
            //const png = await convert(result.data);
            //fs.writeFileSync(svg_out_png, png);

            const outputFilePath = await convertFile(svg_in, {
                width: 50,
                height: 50
            });
        })

    }); 
}