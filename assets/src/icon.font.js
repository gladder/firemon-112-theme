module.exports = {
    files: [
        './svg/*.svg'
    ],
    fontName: 'f112-icons',
    classPrefix: 'f112-',
    baseSelector: '.f112-icon',
    types: ['eot', 'woff', 'woff2', 'ttf', 'svg'],
    fileName: '../fonts/[fontname].[hash].[ext]',
    cssFontsUrl:'../fonts/[fontname].[hash].[ext]',
};