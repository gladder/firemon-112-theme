import { registerRoute } from 'workbox-routing';
import { NetworkFirst } from 'workbox-strategies';
import { CacheableResponsePlugin } from 'workbox-cacheable-response';
//later import { StaleWhileRevalidate } from 'workbox-strategies';
//later import { ExpirationPlugin } from 'workbox-expiration';
import { CacheFirst } from 'workbox-strategies';

// page cache settings
const cacheNamePages = 'pages';
const matchCallbackPages = ({ request }) => request.mode === 'navigate';
const networkTimeoutSeconds = 3;

const warmupUrls = [
    '/monitor',
    '/'
];

// static ressources settings
//later const cacheNameStatic = 'static-resources';
//later const matchCallbackStatic = ({ request }) =>
    // CSS
//later     request.destination === 'style' ||
    // JavaScript
//later     request.destination === 'script' ||
    // Web Workers
//later     request.destination === 'worker';

// image cache settings
//later const cacheNameImages = 'images';
//later const matchCallbackImages = ({ request }) => request.destination === 'image';
//later const maxAgeSeconds = 1 * 60 * 60; // 1h
//later const maxEntries = 60;

const navigationStrategy = new NetworkFirst({
        networkTimeoutSeconds,
        cacheName: cacheNamePages,
        plugins: [
            new CacheableResponsePlugin({
                statuses: [0, 200],
            }),
        ],
    });

registerRoute(
    matchCallbackPages,
    navigationStrategy,
);

/*
for later usage
registerRoute(
    matchCallbackStatic,
    new StaleWhileRevalidate({
        cacheName: cacheNameStatic,
        plugins: [
            new CacheableResponsePlugin({
                statuses: [0, 200],
            }),
        ],
    }),
);

registerRoute(
    matchCallbackImages,
    new CacheFirst({
        cacheName: cacheNameImages,
        plugins: [
            new CacheableResponsePlugin({
                statuses: [0, 200],
            }),
            new ExpirationPlugin({
                maxEntries,
                maxAgeSeconds,
            }),
        ],
    }),
);
*/

self.addEventListener('install', (event) => {
    self.skipWaiting();

    const done = warmupUrls.map(path => navigationStrategy.handleAll({
        event,
        request: new Request(path),
    })[1]);

    event.waitUntil(Promise.all(done));
});
