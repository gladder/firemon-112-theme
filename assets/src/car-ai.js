import CameraPhoto, { FACING_MODES, IMAGE_TYPES } from 'jslib-html5-camera-photo';
import {detectCarReq} from "./util/carnet";

// pass the video element to the constructor.
let videoElement = jQuery('#video');
let imgElement = jQuery('#image');
imgElement.on('load',function(){
    console.log('image loaded - detect car');
    detectCar();
});


// get buttons elements from the html
let takePhotoButtonElement = jQuery('#takePhotoButton');
let stopCameraButtonElement = jQuery('#stopCameraButton');
let startCameraButtonElement = jQuery('#startCameraButton');
let camSelectElement = jQuery('#camSelect');
let detectionResultElement = jQuery('#detection-result');
let imageOverlayElement = jQuery('#imageOverlay');
let videoHolder = jQuery('#videoHolder');
let imageHolder = jQuery('#imageHolder');
let restartButton = jQuery('#restartButton');
let carHolder = jQuery('#carHolder');

// bind the buttons to the right functions.
takePhotoButtonElement.on('click',function(){ takePhoto(); });
stopCameraButtonElement.on('click',function(){ stopCamera(); });
startCameraButtonElement.on('click',function(){ startCamera(); });
restartButton.on('click',function(){
    videoHolder.show();
    imageHolder.hide();
});
camSelectElement.on('change', function() {
    console.log(this.value);
    stopCamera();
    startCamera(this.value);
});

// instantiate CameraPhoto with the videoElement
let cameraPhoto = new CameraPhoto(videoElement[0]);

function takePhoto () {
    console.log("take photo");
    detectionResultElement.html('<span style="color: green;"Detecting car...</span>');
    const config = {
        sizeFactor : 1,
        imageType : IMAGE_TYPES.JPG,
        imageCompression : .90,
        isImageMirror : false,
    }
    let dataUri = cameraPhoto.getDataUri(config);
    imageHolder.show();
    imgElement.attr('src',dataUri);
    detectionResultElement.show();
    videoHolder.hide();
}

function detectCar() {
    imageOverlayElement.attr('width', imgElement.width());
    imageOverlayElement.attr('height', imgElement.height());
    imageOverlayElement.show();

    //console.log("image", imgElement.attr('src'));
    detectCarReq(imgElement.attr('src')).then(
        function(data) {
            console.log("got response", data);
            let detectionString = "";
            let detectionColor = "red";
            let detectionPercentage = "0.0%";
            if (data != null && data.detections != null && data.detections.length > 0) {
                if (data.detections[0].class != null) {
                    if (data.detections[0].class.probability > 0.5) {
                        detectionColor = "orange";
                    }
                    if (data.detections[0].class.probability > 0.75) {
                        detectionColor = "lime";
                    }
                    detectionPercentage = (data.detections[0].class.probability * 100).toFixed(1) + "%";
                }
                if (data.detections[0].box != null) {
                    console.log("draw bounds",data.detections[0].box);
                    const ctx = imageOverlayElement[0].getContext("2d");
                    ctx.beginPath();
                    ctx.lineWidth = "3";
                    ctx.strokeStyle = detectionColor;
                    let x = data.detections[0].box.tl_x * imgElement.width();
                    let y = data.detections[0].box.tl_y * imgElement.height();
                    let w = (data.detections[0].box.br_x - data.detections[0].box.tl_x) * imgElement.width();
                    let h = (data.detections[0].box.br_y - data.detections[0].box.tl_y) * imgElement.height();
                    ctx.rect(x,y,w,h);
                    ctx.stroke();
                }
                if (data.detections[0].mmg != null && data.detections[0].mmg.length > 0) {
                    if (data.detections[0].mmg[0].make_name != null && data.detections[0].mmg[0].make_name.length > 0) {
                        detectionString += '<b>' + data.detections[0].mmg[0].make_name + '</b>';
                    }
                    if (data.detections[0].mmg[0].model_name != null && data.detections[0].mmg[0].model_name.length > 0) {
                        detectionString += ' ' + data.detections[0].mmg[0].model_name;
                    }
                    if (data.detections[0].mmg[0].generation_name != null && data.detections[0].mmg[0].generation_name.length > 0) {
                        detectionString += ' - ' + data.detections[0].mmg[0].generation_name;
                    }
                    if (data.detections[0].mmg[0].years != null && data.detections[0].mmg[0].years.length > 0) {
                        detectionString += ' (' + data.detections[0].mmg[0].years + ')';
                    }
                    detectionString += ' <span style="color: '+detectionColor+';">' + detectionPercentage + '</span>';
                }
            }
            if (detectionString.length <= 0) {
                detectionString = '<span style="color: red;">Detection failed!</span>';
            }
            detectionResultElement.html(detectionString);
        },
        function (err) {
            console.log(err);
            detectionResultElement.html('<span style="color: red;">Detection failed!</span>');
        }
    );
}

function enumerateCameras () {
    console.log("enumerating Cameras");
    cameraPhoto.enumerateCameras()
        .then((cameras)=>{
            let currentCam = cameraPhoto.getCameraSettings();
            camSelectElement.empty();
            cameras.forEach((camera) => {
                let {kind, label, deviceId} = camera;
                camSelectElement.append('<option value="' + deviceId + '" ' + (deviceId == currentCam.deviceId ? ' selected = "selected"' : '') + '>' + label + '</option>');
                console.log(camera);
            });
        })
        .catch((error) => {
            console.log('No cameras to enumerate!:', error);
        });
}

function stopCamera () {
    console.log("stop camera");
    cameraPhoto.stopCamera()
        .then(() => {
            console.log('Camera stoped!');
            startCameraButtonElement.show();
            stopCameraButtonElement.hide();
        })
        .catch((error) => {
            console.log('No camera to stop!:', error);
        });
}

function startCamera(deviceId = "") {
    console.log("start camera");
    if (deviceId.length <= 0) {
        deviceId = FACING_MODES.ENVIRONMENT;
    }
    cameraPhoto.startCamera(deviceId, { width: 1080, height: 720 })
        .then(() => {
            console.log('Camera started !', cameraPhoto.getCameraSettings());
            startCameraButtonElement.hide();
            stopCameraButtonElement.show();
            enumerateCameras();
        })
        .catch((error) => {
            console.error('Camera not started!', error);
        });
}

async function listCarBrands() {
    try {
        // Connect to the MongoDB server
        await client.connect();

        // Get a reference to your database and collection
        const db = client.db('rettungskarten'); // Replace with your database name
        const collection = db.collection('cars'); // Replace with your collection name

        // Find documents
        const findResult = await collection.find({ }).toArray();


        console.log('Find Result:', findResult);
    } catch (err) {
        console.error('Error:', err);
    } finally {
        // Close the connection when done
        await client.close();
    }
}

jQuery(function(){
    imageHolder.hide();
    startCamera();
    listCarBrands();
});