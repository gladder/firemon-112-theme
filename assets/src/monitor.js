import * as constants from './util/constants';
import Device from './classes/Device';
import Station from './classes/Station';
import Gui from './classes/Gui';
import Mq from './classes/Mq';
import Alarm from './classes/Alarm';
import clock from './util/clock';
import * as ident from './util/monitorident';
import * as req from './util/requests';
import * as watchdog from './util/watchdog';
import * as messagequeue from './util/mq';
import * as alarmflow from './util/alarm';
//import html2canvas from "html2canvas"; (it will be loaded only if needed)
import PullToRefresh from 'pulltorefreshjs';

let device = null;
let mq = null;
let station = null;
let gui = null;
let alarm = null;

jQuery(function(){
    device = new Device();
    mq = new Mq();
    station = new Station();
    gui = new Gui();
    alarm = new Alarm();

    // expose to debugger -> disable for PROD build!
    //window.device = device;
    //window.mq = mq;
    //window.station = station;
    //window.gui = gui;
    //window.alarm = alarm;

    // bootstrap
    window.alert_gui_rendered = false;

    gui.hideIdle();
    gui.hideMultiAlertHolder();

    window.addEventListener('online', function(){
        gui.setOnline(true);
        jQuery(document).trigger(constants.EVT_LOOP_MAIN, true); // reinit for force reconnection of all services
    });
    window.addEventListener('offline', function() {
        gui.setOnline(false);
    });

    gui.multi_alert_fab.click(function() {
        gui.multi_alert_box.toggle();
    });

    PullToRefresh.init({
        mainElement: 'body',
        onRefresh() {
            jQuery(document).trigger(constants.EVT_CHECK_ALERT);
            //window.location.reload();
        }
    });
});

jQuery(document).on( 'visibilitychange', function(  ) {
    if (document.visibilityState === "visible") {
        jQuery(document).trigger(constants.EVT_LOOP_MAIN, true); // reinit for PWA that gets visible again to prevent stuck gui
    }
});

jQuery(document).on( constants.EVT_LOOP_CLOCK, function(  ) {
    clock(gui, alarm);
    ident.monitorIndicators(gui, device, mq);
});

jQuery(document).on( constants.EVT_RESET_ALERT, function(  ) {
    alarm = new Alarm();
    window.alert_gui_rendered = false;
});

jQuery(document).on( constants.EVT_DEPESCHE_SPEECH, function(  ) {
    if (gui.gongAudio != null && alarm.speechAudio != null) {
        if (alarm.speech_done_count < device.speech_count) {
            alarm.speech_done_count++;
            gui.gongAudio.onended = function () {
                alarm.speechAudio.play();
                alarm.speechAudio.onended = function () {
                    setTimeout(function(){
                        jQuery(document).trigger(constants.EVT_DEPESCHE_SPEECH);
                    }, device.speech_interval*1000);
                }
            }
            gui.gongAudio.play();
        } else {
            console.error("alarm speech done count gte device speech count");
        }
    } else {
        console.error("no audio for speech available");
    }
});

jQuery(document).on( constants.EVT_ENABLE_ALERT, function( evt, params ) {
    console.log("evt Enable Alert", params.einsatznummer);
    let willSwitch = true;
    if (alarm != null && alarm.active() && device.mobileDevice()) {
        // disallow enabling most recent alert for mobile devices that have alert already running
        willSwitch = false;
    }
    if (alarm != null && alarm.active() && params.einsatzupdate) {
        // if this happens we want to update this in any case
        willSwitch = true;
    }
    if (gui.alarms != null && gui.alarms.length > 0) {
        console.log("willswitch and force", willSwitch, params.force);
        if (willSwitch || params.force) {
            let newAlarm = null;
            for (let i = 0; i < gui.alarms.length; i++) {
                if (gui.alarms[i].id === params.id) {
                    newAlarm = gui.alarms[i];
                    break;
                }
            }

            if (newAlarm != null && newAlarm.active()) {
                alarm = newAlarm;
                alarmflow.enableAlarm(alarm, gui, device, station);
            } else {
                alarmflow.disableAlarm(false, alarm, gui, device);
            }
        }
    } else {
        alarmflow.disableAlarm(false, alarm, gui, device);
    }
    gui.handleMultiAlert(alarm);
    if (alarm.station !== null && alarm.station.alert_feedback_type > 0) {
        jQuery(document).trigger(constants.EVT_UPDATE_ALERT_FEEDBACK);
    } else {
        console.log('did not call feedback endpoint since it is disabled.');
    }
});

jQuery(document).on( constants.EVT_CHECK_ALERT, function(  ) {

    if (device.ready() && station.ready() && ( (device.dependsMq() && mq.ready()) || !device.dependsMq() ) && gmapLoaded) {
        //console.log('do check alert');
        if (injectAlert > 0) {
            // show injectedAlert only!
            req.getAlert(device, injectAlert).then(
                function (injectedAlarm) {
                    if (injectedAlarm != null && injectedAlarm.id != null && injectedAlarm.id > 0) {
                        gui.alarms = [];
                        gui.alarms.push(injectedAlarm);
                        jQuery(document).trigger(constants.EVT_ENABLE_ALERT, { id: injectedAlarm.id, einsatzupdate: injectedAlarm.einsatzupdate, force: false } );
                    } else {
                        gui.alarms = [];
                        gui.handleMultiAlert(alarm);
                        alarmflow.disableAlarm(false, alarm, gui, device);
                    }
                },
                function (err) {
                    console.log(err);
                }
            );
        } else {
            // show latest alert(s)
            req.getRecentAlerts(device).then(
                function (alarms) {
                    if (alarms != null && alarms.length > 0) {
                        gui.alarms = alarms;
                        jQuery(document).trigger(constants.EVT_ENABLE_ALERT, { id: alarms[0].id, einsatzupdate: alarms[0].einsatzupdate, force: false } );
                    } else {
                        gui.alarms = [];
                        gui.handleMultiAlert(alarm);
                        alarmflow.disableAlarm(false, alarm, gui, device);
                    }
                },
                function (err) {
                    console.log(err);
                }
            );
        }
    } else {
        if (device.active()) {
            console.log('not yet ready to check alert - try again in 1.5 s');
            setTimeout(function () {
                jQuery(document).trigger(constants.EVT_CHECK_ALERT);
            }, 1500);
        } else {
            console.log('not yet ready to check alert - will not try since this device is not active');
        }
    }
});

jQuery(document).on( constants.EVT_UPDATE_ALERT_FEEDBACK, function(  ) {
    if (device.ready() && station.ready() && ( (device.dependsMq() && mq.ready()) || !device.dependsMq() ) && alarm.active()) {
        req.getAlertFeedback(device, alarm).then(
            function (feedback) {
                console.log(feedback);
                if (feedback.hasOwnProperty('alert_feedback_types') && feedback.alert_feedback_types.length > 0) {
                    for (let i in feedback.alert_feedback_types) {
                        $('#' + feedback.alert_feedback_types[i].type + ' > span.cnt').html(feedback.alert_feedback_types[i].count + '/' + feedback.alert_overall_people);
                        let ul = $('#' + feedback.alert_feedback_types[i].type + '-extra > ul');
                        ul.empty();
                        if (feedback.alert_feedback_types[i].hasOwnProperty("people")) {
                            if (feedback.alert_feedback_types[i].people.length > 0) {
                                for (let j in feedback.alert_feedback_types[i].people) {
                                    let pills = "";
                                    if (feedback.alert_feedback_types[i].people[j].mission_roles.length > 0) {
                                        for (let k in feedback.alert_feedback_types[i].people[j].mission_roles) {
                                            pills += '<span class="mission_role_pill" style="color: ' + feedback.alert_feedback_types[i].people[j].mission_roles[k].marker_color + '; border-color: ' + feedback.alert_feedback_types[i].people[j].mission_roles[k].marker_color + '; display: inline-block;">' + feedback.alert_feedback_types[i].people[j].mission_roles[k].code + '</span>';
                                        }
                                    }
                                    ul.append('<li>' + pills + ' ' + (!feedback.alert_feedback_types[i].people[j].is_counted_detail ? ' <b>[informell]</b>' : '') + feedback.alert_feedback_types[i].people[j].name + ' (' + feedback.alert_feedback_types[i].people[j].performed + ')</li>');
                                }
                            } else {
                                ul.append('<li>Keine Einsatz-Rückmeldung vorhanden.</li>');
                            }
                        }

                    }
                }
                gui.feedback.html('');
                if (feedback.hasOwnProperty('alert_feedback_groups') && feedback.alert_feedback_groups.length > 0) {
                    feedback.alert_feedback_groups.sort(function(a,b){ return a.prio<b.prio});
                    for (let i in feedback.alert_feedback_groups) {
                        let icon = '<i class="icon icon-user" style="color: white;"></i>';
                        if (feedback.alert_feedback_groups[i].code.length > 0) {
                            icon = '<span class="mission_role_pill" style="color: ' + feedback.alert_feedback_groups[i].marker_color + '; border-color: ' + feedback.alert_feedback_groups[i].marker_color + ';">' + feedback.alert_feedback_groups[i].code + '</span>';
                        }
                        let row = `
                            <div class="row alert_feedback_mission_row">
                            <div class="col-xs-9">
                                <b>` + feedback.alert_feedback_groups[i].count + `</b> ` + feedback.alert_feedback_groups[i].label + `
                            </div>
                            <div class="col-xs-3 text-right">` + icon + `</div>
                        `
                        gui.feedback.append(row);
                    }
                }
            },
            function (err) {
                console.log(err);
            }
        );
    } else {
        if (device.active()) {
            console.log('not yet ready to check alert feedback - try again in 2.5 s');
            setTimeout(function () {
                jQuery(document).trigger(constants.EVT_UPDATE_ALERT_FEEDBACK);
            }, 2500);
        } else {
            console.log('not yet ready to check alert feedback - will not try since this device is not active');
        }
    }
});

jQuery(document).on( constants.EVT_ALERT_GUI_RENDERING_FINISHED, function(  ) {
    // wait for another 5 secs to let gui settle before considered as fully loaded
    setTimeout(() => {
        window.alert_gui_rendered = true;
    }, 5000);

    if (device.print) {
        jQuery(document).trigger(constants.EVT_PRINT_ALERT);
    }
});

jQuery(document).on( constants.EVT_ROUTE_SCREENSHOT, function(  ) {
    if (device.printerCapableDevice()) {
        console.log("route tiles loaded");
        import('html2canvas').then(({ default: html2canvas }) => {
            html2canvas(document.getElementById("route"), {allowTaint: true, useCORS: true}).then(function (canvas) {
                let oImg = document.getElementById("routePrintImg");
                oImg.src = canvas.toDataURL("image/png");
            });
        }).catch(err => {
            console.log('html2canvas failed to load', err);
        });
        console.log("route screenshot done");
    }
});

jQuery(document).on( constants.EVT_MAP_SCREENSHOT, function(  ) {
    if (device.printerCapableDevice()) {
        console.log("map loaded");
        import('html2canvas').then(({ default: html2canvas }) => {
            html2canvas(document.getElementById("map"), {allowTaint: true, useCORS: true}).then(function (canvas) {
                let oImg = document.getElementById("mapPrintImg");
                oImg.src = canvas.toDataURL("image/png");
            });
        }).catch(err => {
            console.log('html2canvas failed to load', err);
        });
        console.log("map screenshot done");
    }
});

jQuery(document).on( constants.EVT_PRINT_ALERT, function(  ) {
    console.log(' ~~~> Print Depesche ' + device.print_count + ' times');
    if (device.watchdogMonitorDevice()) {
        // print by downloading pdf and sending it to lp by console using watchdog
        // TODO: implement call to watchdog
        // leave this stuff for now
        for (let i = 0; i < device.print_count; i++) {
            setTimeout(function () {
                console.log("Print!");
                window.print();
            }, 30000 + (i * 10000));
        }
    } else {
        // print by using browser method (ressource intense and not recommended for weak hardware)
        for (let i = 0; i < device.print_count; i++) {
            setTimeout(function () {
                console.log("Print!");
                window.print();
            }, 30000 + (i * 10000));
        }
    }
});

jQuery(document).on( constants.EVT_LOOP_MAIN, function( evt, firstRun ) {
    if (firstRun) {
        console.log("LOOP MAIN (FIRST run)");
        jQuery(document).trigger(constants.EVT_CHECK_ALERT);
    } else {
        gui.monitorDisplay(device);
        if (gui.needsReload()) {
            jQuery(document).trigger(constants.EVT_GUI_UPDATE_NEEDED);
        }
    }

    req.getMonitorVersion(device).then(
        function(versionData) {
            //console.log(versionData, version)
            gui.setOnline(true); // good sign that we are online ;-)
            if (version !== versionData.result) {
                jQuery(document).trigger(constants.EVT_GUI_UPDATE_NEEDED);
            }
            req.getMonitorConfig(device, version).then(
                function (configData) {
                    device.updateConfig(configData);
                },
                function (err) {
                    console.log(err);
                }
            );
        },
        function(err) {
            gui.setOnline(false); // most common reason when this will fail is bein offline
            console.log(err);
        }
    );
});

jQuery(document).on( constants.EVT_GUI_UPDATE_NEEDED, function(  ) {
    if (alarm.active()) {
        console.log("Restart postponed since there is an alarm active... ");
    } else {
        jQuery(document).trigger(constants.EVT_GUI_RESTART);
    }
});

jQuery(document).on( constants.EVT_GUI_RESTART, function( evt ) {
    console.log("Event: " + evt.type );
    messagequeue.disconnectMq(mq);
    watchdog.disconnectWatchdog(device);
    gui.reload();
});

jQuery(document).on( constants.EVT_DEVICE_CONFIG_UPDATED, function(  ) {
    //console.log("Event: " + evt.type );
    ident.monitorIdent(gui, device, station, mq);
    if (device.wachendisplayEnabled()) {
        gui.enableWachendisplayLayout();
    } else {
        gui.disableWachendisplayLayout();
    }
});

jQuery(document).on( constants.EVT_CALL_WACHENDISPLAY_INIT, function(  ) {
    console.log("Call Wachendisplay Init");
    req.getWachendisplayConfig(device).then(
        function (data) {
            //console.log(data);
            let wd_container = $('div.idle-container');
            if (data.hasOwnProperty('layout') && data.layout.hasOwnProperty('cols') && data.layout.cols.length > 0) {
                for (let i = 0; i < data.layout.cols.length; i++) {
                    let tmp = '<div class="module-col col-w'+data.layout.cols[i]+'">';
                    tmp += '<div class="module-top" data-index="'+((i*2)+1)+'"></div>';
                    tmp += '<div class="module-bottom" data-index="'+((i+1)*2)+'"></div>';
                    tmp += "</div>";
                    wd_container.append(tmp);
                }
            }
            if (data.hasOwnProperty('modules') && data.modules.length > 0) {
                //console.log(data.modules);
                for (let i = 0; i < data.modules.length; i++) {

                    let pos = 1;
                    let refresh_seconds = 60*15; // 15 Min (default)
                    let module_id = "";
                    let cfg = [];
                    if (data.modules[i].hasOwnProperty('position')) {
                        pos = data.modules[i].position;
                    }

                    //if (data.modules[i].hasOwnProperty('cfg') && data.modules[i]['cfg'].hasOwnProperty('reloadIntervalSeconds')) {
                    //    refresh_seconds = Math.max(parseInt(data.modules[i]['cfg'].reloadIntervalSeconds), 60); // not faster than 60s!
                    //}

                    if (data.modules[i].hasOwnProperty('reloadIntervalSeconds')) {
                        refresh_seconds = Math.max(parseInt(data.modules[i].reloadIntervalSeconds), 60); // not faster than 60s!
                    }

                    if (data.modules[i].hasOwnProperty('cfg')) {
                        cfg = data.modules[i].cfg;
                    }

                    if (data.modules[i].hasOwnProperty('id')) {
                        module_id = data.modules[i].id;
                    }
                    if (module_id.length > 0) {
                        //console.log("Module Content", data.modules[i]);
                        $('div.idle-container > div.module-col > div[data-index="' + pos + '"]').append('<div data-module-id="' + data.modules[i].id + '">' + data.modules[i].id + '</div>');
                        setTimeout(function () {
                            jQuery(document).trigger(constants.EVT_CALL_WACHENDISPLAY_MODULES, { module_id: module_id, refresh_seconds: refresh_seconds, cfg: cfg } );
                        }, 1500 * i);
                    }
                }
            }
            if (data.hasOwnProperty('todays_slots_time_series') && data.todays_slots_time_series.length > 0) {
                device.wachendisplay_todays_slots_time_series = data.todays_slots_time_series;
            }
            gui.monitorDisplay(device); // take care to enable display again since it may be disabled if no alert
        },
        function (err) {
            console.log(err);
        }
    );
});

jQuery(document).on( constants.EVT_CALL_WACHENDISPLAY_MODULES, function( evt, payload ) {
    console.log("Call Wachendisplay Modules Update for " + payload.module_id + " (every "+payload.refresh_seconds+" seconds)");
    // handle module rendering
    req.getRenderedModule(device, payload.module_id, payload.cfg).then(
        function (data) {
            //console.log(data);
            let module_container = $('div[data-module-id="'+payload.module_id+'"]');
            if (module_container.length > 0) {
                module_container.html(data.result);
            }
        },
        function (err) {
            console.log(err);
        }
    );
    // reschedule next update
    setTimeout(function () {
        jQuery(document).trigger(constants.EVT_CALL_WACHENDISPLAY_MODULES, payload);
    }, payload.refresh_seconds * 1000);
});

jQuery(document).on( constants.EVT_STATION_CHANGED, function(  ) {
    //console.log("Event: " + evt.type );
    req.getStationConfig(device).then(
        function(data) {
            station.update(data);
            ident.monitorIdent(gui, device, station, mq);
        },
        function(err) {
            console.log(err);
        }
    );
});

jQuery(document).on( constants.EVT_STATE_CHANGED, function(  ) {
    //console.log("Event: " + evt.type );
    ident.monitorIdent(gui, device, station, mq);
    if (device.active()) {
        gui.hideError();
        gui.showIdle();
        gui.mainLoopTimingNormal();
        jQuery(document).trigger(constants.EVT_CHECK_ALERT);
    } else {
        gui.hideIdle();
        if (device.pending()) {
            gui.showError(device.name, "icon-spin icon-spinner");
        } else {
            gui.showError(device.state, "icon-spin icon-spinner");
            gui.mainLoopTimingNormal();
        }
    }

});

jQuery(document).on( constants.EVT_DEVICE_WATCHDOG_UPDATED, function( evt ) {
    console.log("Event: " + evt.type );
    watchdog.connectDevice(device);
});

jQuery(document).on( constants.EVT_TOGGLE_DISPLAY, function( evt, flag ) {
    console.log("Event: " + evt.type );
    watchdog.toggleDisplay(device, flag);
});

jQuery(document).on( constants.EVT_DEVICE_RESOLUTION_UPDATED, function( evt ) {
    console.log("Event: " + evt.type );
    console.log("Device-Resolution is: " + device.resolution.width + " x " + device.resolution.width);
});

jQuery(document).on( constants.EVT_MQ_CHANGED, function( evt ) {
    console.log("Event: " + evt.type );

    if (device.active()) {
        messagequeue.disconnectMq(mq);
        req.getMqConfig(device).then(
            function (data) {
                mq.update(data);
                jQuery(document).trigger(constants.EVT_MQ_CONNECT);
            },
            function (err) {
                console.log(err);
            }
        );
    } else {
        messagequeue.disconnectMq(mq);
    }
});

jQuery(document).on( constants.EVT_MQ_CONNECT, function( evt ) {
    console.log("Event: " + evt.type );
    messagequeue.connectMq(device, mq, gui);
});

jQuery(document).on( constants.EVT_TRIGGER_GPIO_ALERT, function( evt, gpio_trigger_id ) {
    console.log("Event: " + evt.type );
    req.triggerGpioAlert(device, gpio_trigger_id).then(
        function(data) {
            console.log("TRIGGERED GPIO ALERT",gpio_trigger_id);
        },
        function(err) {
            console.log(err);
        }
    );
});
