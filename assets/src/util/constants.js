export const STATE_UNKNOWN = "unknown";
export const STATE_PENDING = "pending";
export const STATE_ACTIVE = "active";
export const STATE_DISABLED = "disabled";

export const CLIENT_TYPE_ANDROID = "android";
export const CLIENT_TYPE_IOS = "ios";
export const CLIENT_TYPE_WEB = "web";

export const TYPE_MONITOR = "monitor";
export const TYPE_PAGER = "pager";
export const TYPE_MOBILE = "mobile";

export const EVT_CHECK_ALERT = "check-alert";
export const EVT_PRINT_ALERT = "print-alert";
export const EVT_ALERT_GUI_RENDERING_FINISHED = "alert-gui-rendering-finished";
export const EVT_ENABLE_ALERT = "enable-alert";
export const EVT_RESET_ALERT = "reset-alert";
export const EVT_STATE_CHANGED = "state-changed";
export const EVT_STATION_CHANGED = "station-changed";
export const EVT_MQ_CHANGED = "mq-changed";
export const EVT_MQ_CONNECT = "mq-connect";
export const EVT_LOOP_CLOCK = "loop-clock";
export const EVT_LOOP_MAIN = "loop-main";
export const EVT_GUI_UPDATE_NEEDED = "gui-update-needed";
export const EVT_DEVICE_CONFIG_UPDATED = "device-config-updated";
export const EVT_DEVICE_WATCHDOG_UPDATED = "device-watchdog-updated";
export const EVT_GUI_RESTART = "gui-restart";

export const EVT_CALL_WACHENDISPLAY_INIT = "wachendisplay-init";

export const EVT_CALL_WACHENDISPLAY_MODULES = "wachendisplay-modules";
export const EVT_TOGGLE_DISPLAY = "toggle-display";
export const EVT_DEPESCHE_SPEECH = "depesche-speech";
export const EVT_DEVICE_RESOLUTION_UPDATED = "device-resolution-updated";
export const EVT_MAP_SCREENSHOT = "screenshot-route";
export const EVT_ROUTE_SCREENSHOT = "screenshot-map";
export const EVT_UPDATE_ALERT_FEEDBACK = "update-alert-feedback";

export const GUI_STATE_IDLE = "idle";
export const GUI_STATE_ALERT = "alert";

export const SVG_FIRE_SITE = "M14.66 14.18C14.69 14.29 14.7 14.4 14.7 14.5C14.73 15.15 14.44 15.85 13.97 16.28C13.75 16.47 13.39 16.67 13.11 16.75C12.23 17.06 11.35 16.62 10.83 16.11C11.77 15.89 12.32 15.21 12.5 14.5C12.62 13.89 12.37 13.38 12.27 12.78C12.17 12.2 12.19 11.71 12.4 11.18C12.55 11.47 12.71 11.77 12.9 12C13.5 12.78 14.45 13.12 14.66 14.18M22 12C22 17.5 17.5 22 12 22S2 17.5 2 12 6.5 2 12 2 22 6.5 22 12M17.16 12.56L17.06 12.36C16.9 12 16.45 11.38 16.45 11.38C16.27 11.15 16.05 10.94 15.85 10.74C15.32 10.27 14.73 9.94 14.22 9.45C13.05 8.31 12.79 6.44 13.54 5C12.79 5.18 12.14 5.58 11.58 6.03C9.55 7.65 8.75 10.5 9.71 12.95C9.74 13.03 9.77 13.11 9.77 13.21C9.77 13.38 9.65 13.53 9.5 13.6C9.31 13.67 9.13 13.63 9 13.5C8.93 13.46 8.9 13.42 8.87 13.37C8 12.26 7.84 10.66 8.43 9.39C7.12 10.45 6.41 12.24 6.5 13.92C6.56 14.31 6.6 14.7 6.74 15.09C6.85 15.56 7.06 16 7.3 16.44C8.14 17.78 9.61 18.75 11.19 18.94C12.87 19.15 14.67 18.85 15.96 17.7C17.4 16.4 17.9 14.33 17.16 12.56Z";
export const SVG_FIRE_STATION = "M19.07,4.93C17.22,3 14.66,1.96 12,2C9.34,1.96 6.79,3 4.94,4.93C3,6.78 1.96,9.34 2,12C1.96,14.66 3,17.21 4.93,19.06C6.78,21 9.34,22.04 12,22C14.66,22.04 17.21,21 19.06,19.07C21,17.22 22.04,14.66 22,12C22.04,9.34 21,6.78 19.07,4.93M17,12V18H13.5V13H10.5V18H7V12H5L12,5L19.5,12H17Z";
export const SVG_GPS_DESTINATION = "M12,8A4,4 0 0,1 16,12A4,4 0 0,1 12,16A4,4 0 0,1 8,12A4,4 0 0,1 12,8M3.05,13H1V11H3.05C3.5,6.83 6.83,3.5 11,3.05V1H13V3.05C17.17,3.5 20.5,6.83 20.95,11H23V13H20.95C20.5,17.17 17.17,20.5 13,20.95V23H11V20.95C6.83,20.5 3.5,17.17 3.05,13M12,5A7,7 0 0,0 5,12A7,7 0 0,0 12,19A7,7 0 0,0 19,12A7,7 0 0,0 12,5Z";

export const EVT_TRIGGER_GPIO_ALERT = "trigger-gpio-alert";