import {createParagraph} from "./htmlgen";

export function monitorIdent(gui, device, station, mq) {
    gui.monitor_ident.html(device.name + " v"+version);

    gui.idle_dialog.find('.idle-ident').html("<p>" + device.name + " (v"+version + (device.watchdog_version != null && device.watchdog_version.length>0 ? "wd-"+device.watchdog_version : "") + ")</p>");
    gui.idle_dialog.find('.idle-ident').empty().append(
        createParagraph({
            text: device.name + " (v"+version + (device.watchdog_version != null && device.watchdog_version.length>0 ? "wd-"+device.watchdog_version : "") + ")"
        })
    );

    gui.idle_dialog.find('.idle-station').empty().append(
        createParagraph({ text: station.name })
    );

    if (device.ips != null && device.ips.length > 0) {
        gui.idle_dialog.find('.idle-station').append(
            createParagraph({ class: 'ips', text: device.ips.join(", ") })
        );
    }
    monitorIndicators(gui, device, mq);

    if (device.background != null) {
        gui.idle_dialog.css("background-image", "url('" + device.background + "')");
        gui.error_dialog.css("background-image", "url('" + device.background + "')");
    }
}

export function monitorIndicators(gui, device, mq) {
    if (device.active()) {
        gui.deviceIndicator.attr('class','f112-icon f112-check-circle icon-okay');
    }

    if (device.pending()) {
        gui.deviceIndicator.attr('class','f112-icon f112-hourglass icon-neutral');
    }

    if (device.disabled()) {
        gui.deviceIndicator.attr('class','f112-icon f112-x-circle icon-error');
    }

    if (device.dependsWatchdog()) {
        gui.wdIndicator.show();
        if (device.watchdocConnected()) {
            gui.wdIndicator.attr('class','f112-icon f112-shield-check icon-okay');
        } else {
            gui.wdIndicator.attr('class','f112-icon f112-shield-slash icon-error');
        }
    } else {
        gui.wdIndicator.hide();
        gui.wdIndicator.attr('class','f112-icon f112-shield icon-neutral');
    }

    if (device.dependsMq()) {
        gui.mqIndicator.show();
        if (mq.connected()) {
            gui.mqIndicator.attr('class','f112-icon f112-cloud-check icon-okay');
        } else {
            gui.mqIndicator.attr('class','f112-icon f112-cloud-slash icon-error');
        }
    } else {
        if (mq.connected()) {
            gui.mqIndicator.show();
            gui.mqIndicator.attr('class','f112-icon f112-cloud-check icon-okay');
        } else {
            gui.mqIndicator.hide();
            gui.mqIndicator.attr('class','f112-icon f112-cloud icon-neutral');
        }
    }

    if (gui.online) {
        gui.wwwIndicator.attr('class','f112-icon f112-wifi icon-okay');
    } else {
        gui.wwwIndicator.attr('class','f112-icon f112-wifi-off icon-error');
    }
}