import * as req from './requests';
import * as watchdog from './watchdog';
import * as constants from '../util/constants';

export function connectMq(device, mq, gui) {

    if (device.dependsMq()) {
        gui.showError("connecting...", "icon-spin icon-spinner");
    }
    if (mq.id > 0 && mq.host != null && mq.host.length > 0 && mq.port > 0) {
        //console.log("Connect to MQ "+mq.id+" // " + mq.host);
        mq.client = new paho.Client(mq.host, Number(mq.port), "/ws", device.uuid + "-" + Date.now());
        mq.client.onConnectionLost = () => {
            if (device.mq_reconnect_attempts > 0) {
                // console.log("try reconnect MQ ("+device.mq_reconnect_attempts+" attempts left)");
                device.mq_reconnect_attempts--;
                setTimeout(function(){
                    jQuery(document).trigger(constants.EVT_MQ_CONNECT);
                }, 5000);
            } else {
                device.mq_id = 0; // force reconfig on next GetConfig
                disconnectMq(mq);
                //req.reportError(device, "onConnectionLost:"+err.errorMessage, "mq.js", 0, 0, err.errorCode);
            }
        };
        mq.client.onMessageArrived = (msg) => {
            console.log("Message received:"+msg.payloadString);
            if (msg.payloadString == "ALERT") {
                // allow 5 secs to get backend finished
                setTimeout(function(){
                    jQuery(document).trigger(constants.EVT_CHECK_ALERT);
                }, 5000);
            }
            if (msg.payloadString == "REBOOT") {
                watchdog.reboot(device);
            }
            if (msg.payloadString == "REFRESH") {
                jQuery(document).trigger(constants.EVT_GUI_RESTART);
            }
            if (msg.payloadString == "SHUTDOWN") {
                watchdog.shutdown(device);
            }
            if (msg.payloadString == "DISPLAY_ON") {
                watchdog.toggleDisplay(device, true);
            }
            if (msg.payloadString == "DISPLAY_OFF") {
                watchdog.toggleDisplay(device, false);
            }
            if (msg.payloadString == "SELFUPDATE") {
                watchdog.self_update(device);
            }
            if (msg.payloadString == "FEEDBACK-PERFORMED") {
                jQuery(document).trigger(constants.EVT_UPDATE_ALERT_FEEDBACK);
                watchdog.self_update(device);
            }
        };

        mq.client.connect({
            onSuccess: ()  => {
                console.log("Connected!");
                if (device.user_id != null) {
                    let user_sub = 'user-sub-'+device.user_id;
                    console.log("subscribing as user to "+user_sub);
                    mq.client.subscribe(user_sub);
                } else {
                    mq.client.subscribe(mq.vhost);
                    console.log("subscribing to whole station "+mq.vhost);
                }
                device.mq_id = mq.id; // proof that mq_id is set to prevent reconnect loop in edge cases
                device.mq_reconnect_attempts = 3; // in case of connection lost try to reconnect 3 times
                gui.hideError();
            },
            userName : mq.username,
            password : mq.password,
            useSSL: mq.use_ssl,
            onFailure: (err) => {
                if (device.mq_reconnect_attempts > 0) {
                    console.log("try to reconnect MQ ("+device.mq_reconnect_attempts+" attempts left)");
                    device.mq_reconnect_attempts--;
                    setTimeout(function(){
                        jQuery(document).trigger(constants.EVT_MQ_CONNECT);
                    }, 5000);
                } else {
                    device.mq_id = 0; // force reconfig on next GetConfig
                    disconnectMq(mq);
                    req.reportError(device, "onFailure:" + err.errorMessage, "mq.js", 0, 0, err.errorCode);
                }
            },
            timeout: mq.timeout
        });

    } else {
        //showError("no MQ config", "icon-question-sign");
        console.log("no MQ config...");
    }
}

export function disconnectMq(mq) {
    console.log("Disconnect from MQ");
    if (mq.connected()) {
        mq.client.disconnect();
        mq.client = null;
    }
}