import Alarm from '../classes/Alarm';

export async function getMonitorVersion(device) {
    console.log("call getMonitorVersion");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetMonitorVersion', {
            success: function (data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'getMonitorVersion', 0, 0, status);
                reject('Ajax Error while calling getMonitorVersion! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getMonitorConfig(device, currentVersion) {
    console.log("call getMonitorConfig");
    return new Promise(function(resolve, reject)
    {
        let ip = "";
        if (device.ips != null && device.ips.length > 0) {
            ip = device.ips[0];
        }
        let res = "";
        if (device.resolution != null) {
            res = device.resolution.width + "x" + device.resolution.height;
        }
        $.request('onGetConfig', {
            data: { token: device.uuid, type:device.type, gui_version: currentVersion, wd_version: device.watchdog_version, resolution: res, last_known_ip: ip },
            success: function(data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'getMonitorConfig', 0, 0, status);
                reject('Ajax Error while calling getMonitorConfig! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getStationConfig(device) {
    console.log("call getStationConfig");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetStationConfig', {
            data: { token: device.uuid},
            success: function(data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'getStationConfig', 0, 0, status);
                reject('Ajax Error while calling getStationConfig! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getAlertFeedback(device, alert) {
    console.log("call getAlertFeedback");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetAlertFeedback', {
            data: { token: device.uuid, alert_id: alert.id},
            success: function(data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'getAlertFeedback', 0, 0, status);
                reject('Ajax Error while calling getAlertFeedback! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getMqConfig(device) {
    console.log("call getMqConfig");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetMqConfig', {
            data: { token: device.uuid },
            success: function (data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'getStationConfig', 0, 0, status);
                reject('Ajax Error while calling getStationConfig! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getWachendisplayConfig(device) {
    console.log("call getWachendisplayConfig");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetWachendisplayConfig', {
            data: { token: device.uuid },
            success: function (data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'onGetWachendisplayConfig', 0, 0, status);
                reject('Ajax Error while calling onGetWachendisplayConfig! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getRenderedModule(device, module_id, cfg) {
    console.log("call getRenderedModule");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetRenderedModule', {
            data: { token: device.uuid, module_id: module_id, cfg: cfg },
            success: function (data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'getRenderedModule', 0, 0, status);
                reject('Ajax Error while calling getRenderedModule! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export function reportError(device, msg, url, lineNo, columnNo, error) {
    console.error(msg, url, error);
    $.request('onPostError', {
        data: { token: device.uuid, msg: msg, url: url, lineNo: lineNo, columnNo: columnNo, error: error },
        success: function() {
            console.log("Error sent");
        },
        error: function() {
            console.log('Ajax Error! Error not sent');
        }
    });
}

export async function getRecentAlert(device) {
    //console.log("call getRecentAlert");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetRecentAlert', {
            data: {token: device.uuid},
            success: function (data) {
                let result = [];

                if (data.depesche != null) {
                    result = new Alarm();
                    result.id = data.id;
                    result.depesche = data.depesche;
                    result.weather = data.weather;
                    result.station = data.station;
                }

                resolve(result);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'getRecentAlert', 0, 0, status);
                reject('Ajax Error while calling getRecentAlert! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getAlert(device, alert_id) {
    console.log("call getAlert");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetAlert', {
            data: {token: device.uuid, alert_id: alert_id},
            success: function (data) {
                let result = [];

                if (data.depesche != null) {
                    result = new Alarm();
                    result.id = data.id;
                    result.depesche = data.depesche;
                    result.weather = data.weather;
                    result.station = data.station;
                }

                resolve(result);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'getAlert', 0, 0, status);
                reject('Ajax Error while calling getAlert! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getRecentAlerts(device) {
    //console.log("call getRecentAlerts (all recent)");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetRecentAlerts', {
            data: {token: device.uuid},
            success: function (data) {
                let result = [];
                for (let i = 0; i < data.length; i++) {
                    if (data[i].depesche != null) {
                        let a = new Alarm();
                        a.id = data[i].id;
                        a.depesche = data[i].depesche;
                        a.weather = data[i].weather;
                        a.station = data[i].station;
                        result.push(a);
                    }
                }
                // mark updates (same einsatznummer, differend ids)
                for (let i = 0; i < result.length; i++) {
                    for (let j = 0; j < result.length; j++) {
                        if (j>i && result[i].einsatznummer() == result[j].einsatznummer() && !result[j].einsatzupdate) {
                            result[i].einsatzupdate = true;
                        }
                    }
                }
                resolve(result);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'getRecentAlerts', 0, 0, status);
                reject('Ajax Error while calling getRecentAlerts! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getHydrantsNearby(device, alarm) {
    console.log("call getHydrantsNearby");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetHydrantsNearbyV2', {
            data: { long: alarm.long(), lat: alarm.lat(), max_hydrants: 25, coord_delta: 0.015}, // ~ 1500m
            success: function (data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'getHydrantsNearby', 0, 0, status);
                reject('Ajax Error while calling getHydrantsNearby! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getAssemblyAreasNearby(device, alarm) {
    console.log("call getAssemblyAreasNearby");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetAssemblyAreasNearby', {
            data: { long: alarm.long(), lat: alarm.lat(), coord_delta: 0.03}, // ~ 3000m
            success: function (data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'getAssemblyAreasNearby', 0, 0, status);
                reject('Ajax Error while calling getAssemblyAreasNearby! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getEinsatzobjekteNearby(device, alarm) {
    console.log("call getEinsatzobjekteNearby");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetEinsatzobjekteNearby', {
            data: { token: device.uuid, long: alarm.long(), lat: alarm.lat(), coord_delta: 0.0030, alert_id: alarm.id }, // ~ 325m
            success: function (data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'onGetEinsatzobjekteNearby', 0, 0, status);
                reject('Ajax Error while calling onGetEinsatzobjekteNearby! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getRescuePointsNearby(device, alarm) {
    console.log("call getRescuePointsNearby");
    return new Promise(function(resolve, reject)
    {
        $.request('onGetRescuePointsNearby', {
            data: { long: alarm.long(), lat: alarm.lat(), coord_delta: 0.075}, // ~7500m
            success: function (data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'getRescuePointsNearby', 0, 0, status);
                reject('Ajax Error while calling getRescuePointsNearby! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function getWeather(device, alarm) {
    console.log("call getWeather");
    return new Promise(function(resolve, reject)
    {
        let lon = alarm.long();
        let lat = alarm.lat();
        let location_url = 'https://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey='+accu_weather_api_key+'&q='+lat+','+lon+'&language=de-de&details=false&toplevel=false';
        $.ajax( location_url ).done(function(loc_data) {
            let location = loc_data.Key;
            console.log("got location Key: "+ location);
            if (location.length > 0) {
                // get weather
                let weather_url = 'https://dataservice.accuweather.com/currentconditions/v1/'+location+'?apikey='+accu_weather_api_key+'&language=de-de&details=true';
                $.ajax( weather_url ).done(function(weather_data) {
                    if (weather_data.length > 0) {
                        resolve(weather_data[0]);
                    } else {
                        reportError(device, "No weather data for alarm " + alarm.id, 'request.js -> getWeather', 0, 0, "");
                        reject("No weather data for alarm " + alarm.id);
                    }
                });
            } else {
                reportError(device, "No location key for alarm " + alarm.id, 'request.js -> getWeather', 0, 0, "");
                reject("No location key for alarm " + alarm.id);
            }
        });
    });
}

export async function synthDepesche(device, alarm, station) {
    return new Promise(function(resolve, reject)
    {
        try {
            AWS.config.region = 'eu-central-1';
            AWS.config.credentials = new AWS.CognitoIdentityCredentials({IdentityPoolId: aws_polly_identity_pool_id});

            console.log(" ~~~> Synth Depesche");
            if (alarm.depesche !== null) {
                // Create the JSON parameters for getSynthesizeSpeechUrl
                let depescheSpeechText = station.depescheSpeechText(alarm);
                let speechParams = {
                    OutputFormat: "mp3",
                    SampleRate: "16000",
                    Text: depescheSpeechText,
                    TextType: "text",
                    VoiceId: "Marlene"
                };

                console.log("Polly says: " + depescheSpeechText);
                reportError(device, "Polly says: " + depescheSpeechText, "request.js -> synthDepesche", 0, 0, "");
                // Create the Polly service object and presigner object
                let polly = new AWS.Polly({apiVersion: '2016-06-10'});
                let signer = new AWS.Polly.Presigner(speechParams, polly)

                // Create presigned URL of synthesized speech file
                signer.getSynthesizeSpeechUrl(speechParams, function (error, url) {
                    if (error) {
                        reject(error);
                        reportError(device, error, 'request.js -> synthDepesche', 0, 0,"");
                    } else {
                        console.log(url);
                        resolve(url);
                    }
                });
            }
        } catch (e) {
            reject('request.js -> synthDepesche');
            reportError(device, e.message, 'request.js -> synthDepesche', 0, 0, e.name);
        }
    });
}

export async function loadAudio(device, src) {
    return new Promise(function(resolve, reject)
    {
        try {
            console.log("load " + src);
            let audio = new Audio(src);
            audio.load();
            // = function() {
                resolve(audio);
            //};
            //audio.onerror = function() {
            //    reject("Error loadAudio");
            //};
        } catch (e) {
            reject('requests.js -> loadAudio');
            reportError(device, e.message, 'requests.js -> loadAudio', 0, 0, e.name);
        }
    });
}

export async function loginUserToken(email, password, token) {
    console.log("call loginUserToken");
    return new Promise(function(resolve, reject)
    {
        $.request('onLoginUserToken', {
            data: { email: email, password: password, token: token},
            success: function (data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reject('Ajax Error while calling loginUserToken! E-Mail: ' + email + ' - Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}

export async function triggerGpioAlert(device, gpio_alert_trigger_id) {
    console.log("call triggerGpioAlert");
    return new Promise(function(resolve, reject)
    {
        $.request('onTriggerGpioAlert', {
            data: { token: device.uuid, gpio_alert_trigger_id: gpio_alert_trigger_id },
            success: function(data) {
                resolve(data);
            },
            error: function (jq, status, message) {
                reportError(device, message, 'triggerGpioAlert', 0, 0, status);
                reject('Ajax Error while calling onTriggerGpioAlert! Status: ' + status + ' - Message: ' + message);
            }
        });
    });
}