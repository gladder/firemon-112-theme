//import {reportError} from "./requests";

import {reportError} from "./requests";

export async function detectCarReq(imageData) {
    return new Promise(function(resolve, reject)
    {
        try {
            console.log("detect Car");
            //resolve(mockResponse()); // only für debugging response (saves api calls)
            $.request('onDetectCar', {
                data: { imageData: imageData },
                success: function (data) {
                    resolve(data);
                },
                error: function (jq, status, message) {
                    reject('Ajax Error! Status: ' + status + ' - Message: ' + message);
                }
            });
        } catch (e) {
            reject('carnet.js -> detectCar');
        }
    });
}

function mockResponse() {
    return JSON.parse(`
        {
            "detections": [
                {
                    "angle": [],
                    "box": {
                        "br_x": 0.9679,
                        "br_y": 0.8707,
                        "tl_x": 0.048,
                        "tl_y": 0.1383
                    },
                    "class": {
                        "name": "car",
                        "probability": 0.8735
                    },
                    "color": [],
                    "mm": [
                        {
                            "make_id": 4,
                            "make_name": "Audi",
                            "model_id": 72,
                            "model_name": "80",
                            "probability": 0.9983
                        }
                    ],
                    "mmg": [
                        {
                            "generation_id": 77,
                            "generation_name": "V (B4)",
                            "make_id": 4,
                            "make_name": "Audi",
                            "model_id": 72,
                            "model_name": "80",
                            "probability": 0.9983,
                            "years": "1991-1996"
                        }
                    ],
                    "status": {
                        "code": 0,
                        "message": "",
                        "selected": true
                    },
                    "subclass": [
                        {
                            "name": "vehicle",
                            "probability": 1
                        }
                    ]
                },
                {
                    "angle": [],
                    "box": {
                        "br_x": 0.4207,
                        "br_y": 0.3577,
                        "tl_x": 0.0756,
                        "tl_y": 0.177
                    },
                    "class": {
                        "name": "car",
                        "probability": 0.8737
                    },
                    "color": [],
                    "mm": [],
                    "mmg": [],
                    "status": {
                        "code": 23,
                        "message": "Box rejected. Height is 87 which is less than box_min_height 180",
                        "selected": false
                    },
                    "subclass": []
                },
                {
                    "angle": [],
                    "box": {
                        "br_x": 0.999,
                        "br_y": 0.3691,
                        "tl_x": 0.8662,
                        "tl_y": 0.1776
                    },
                    "class": {
                        "name": "car",
                        "probability": 0.3524
                    },
                    "color": [],
                    "mm": [],
                    "mmg": [],
                    "status": {
                        "code": 24,
                        "message": "Box rejected. Width/Height ratio is 0.92 which is less than box_min_ratio 1.0",
                        "selected": false
                    },
                    "subclass": []
                },
                {
                    "angle": [],
                    "box": {
                        "br_x": 0.9966,
                        "br_y": 0.2757,
                        "tl_x": 0.8772,
                        "tl_y": 0.1895
                    },
                    "class": {
                        "name": "car",
                        "probability": 0.3363
                    },
                    "color": [],
                    "mm": [],
                    "mmg": [],
                    "status": {
                        "code": 22,
                        "message": "Box rejected. Width is 76 which is less than box_min_width 180",
                        "selected": false
                    },
                    "subclass": []
                }
            ],
            "is_success": true,
            "meta": {
                "classifier": 2256,
                "md5": "7C5F01047E18FA6382553E3DC9D99F14",
                "parameters": {
                    "box_max_ratio": 3.15,
                    "box_min_height": 180,
                    "box_min_ratio": 1,
                    "box_min_width": 180,
                    "box_select": "center",
                    "features": [
                        "mm",
                        "mmg"
                    ],
                    "region": [
                        "DEF"
                    ]
                },
                "time": 0.219
            }
        }
    `);
}