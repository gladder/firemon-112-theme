export function createLink(params = {}) {
    let link = $('<a/>');
    if (params.href) {
        link.attr('href', params.href);
    }
    if (params.target) {
        link.attr('target', params.target);
    }
    return extendCommonParams(link, params);
}

export function createImage(params = {}) {
    let img = $('<img/>');
    if (params.src) {
        img.attr('src', params.src);
    }
    if (params.alt) {
        img.attr('alt', params.alt);
    }
    if (params.width) {
        img.attr('width', params.width);
    }
    if (params.height) {
        img.attr('height', params.height);
    }
    return img;
}

export function createParagraph(params = {}) {
    let p = $('<p/>');
    return extendCommonParams(p, params);
}

export function createBold(params = {}) {
    let b = $('<b/>');
    if (params.text) {
        b.text(params.text);
    }
    return b;
}

export function createI(params = {}) {
    let i = $('<i/>');
    return extendCommonParams(i, params);
}

export function createDiv(params = {}) {
    let div = $('<div/>');
    return extendCommonParams(div, params);
}

export function createUl(params = {}) {
    let ul = $('<ul/>');
    return extendCommonParams(ul, params);;
}

export function createLi(params = {}) {
    let li = $('<li/>');
    return extendCommonParams(li, params);;
}

export function createH1(params = {}) {
    let headline = $('<h1/>');
    return extendCommonParams(headline, params);
}

export function createH2(params = {}) {
    let headline = $('<h2/>');
    return extendCommonParams(headline, params);
}

export function createH3(params = {}) {
    let headline = $('<h3/>');
    return extendCommonParams(headline, params);
}

export function createH4(params = {}) {
    let headline = $('<h4/>');
    return extendCommonParams(headline, params);
}

export function createSpan(params = {}) {
    let span = $('<span/>');
    return extendCommonParams(span, params);
}

function extendCommonParams(obj, params = {}) {
    if (params.id) {
        obj.attr('id', params.id);
    }
    if (params.class) {
        obj.addClass(params.class);
    }
    if (params.text) {
        obj.text(params.text);
    }
    if (params.html) {
        obj.html(params.html);
    }
    if (params.prepend) {
        obj.prepend(params.prepend);
    }
    if (params.append) {
        obj.append(params.append);
    }
    if (params.data) {
        for (let key in params.data) {
            obj.attr(key, params.data[key]);
        }
    }
    if (params.styles) {
        obj.css(params.styles);
    }
    return obj;
}