import * as req from './requests';
import * as constants from "./constants";

export function connectDevice(device) {
    if (device.dependsWatchdog()) {
        disconnectWatchdog(device);
        device.io_watchdog = io("ws://localhost:3000",{ reconnectionDelay: 2500, secure: true });

        device.io_watchdog.on("connect_error", function () {
            console.log("Watchdog connection error");
        });

        device.io_watchdog.on("connect", function () {
            console.log("Watchdog connected!");
            if (device.gpio_alert_triggers && device.gpio_alert_triggers.length > 0) {
                device.io_watchdog.emit('GPIOTRIGGER', device.gpio_alert_triggers);
            }
        });

        device.io_watchdog.on('HEARTBEAT', function (msg) {
            //console.log("received HEARTBEAT: "+msg);
            if (msg == 'request') {
                device.io_watchdog.emit('HEARTBEAT','alive');
            }
        });
        device.io_watchdog.on('IPS', function (msg) {
            //console.log("received IPS: "+msg);
            device.ips = msg;
            jQuery(document).trigger(constants.EVT_DEVICE_CONFIG_UPDATED);
        });
        device.io_watchdog.on('GPIO_TRIGGER', function (msg) {
            console.log("received GPIO_TRIGGER: "+msg);
            jQuery(document).trigger(constants.EVT_TRIGGER_GPIO_ALERT, parseInt(msg));
        });
        device.io_watchdog.on('VERSION', function (msg) {
            //console.log("received IPS: "+msg);
            if (device.watchdog_version != msg) {
                device.watchdog_version = msg;
                jQuery(document).trigger(constants.EVT_DEVICE_CONFIG_UPDATED);
                jQuery(document).trigger(constants.EVT_LOOP_MAIN, false);
            }
        });
        device.io_watchdog.on('RESOLUTION', function (msg) {
            //console.log("received IPS: "+msg);
            if (typeof msg === 'object' && msg !== null) {
                if (device.resolution == null || (device.resolution.width != msg.width && device.resolution.height != msg.height)) {
                    device.resolution = msg;
                    jQuery(document).trigger(constants.EVT_DEVICE_RESOLUTION_UPDATED);
                    jQuery(document).trigger(constants.EVT_LOOP_MAIN, false);
                }
            }
        });
        device.io_watchdog.on('RESETIDENT', function (msg) {
            console.log("RESETIDENT: "+msg);
            if (msg == 'prepare') {
                localStorage.setItem('reset_uuid', true);
            } else {
                localStorage.removeItem('uuid');
                gui.reload();
            }
        });
    } else {
        disconnectWatchdog(device);
    }
}

export function disconnectWatchdog(device) {
    if (device.io_watchdog != null) {
        try {
            device.io_watchdog.close();
            device.io_watchdog = null;
        } catch {}
    }
}

export function toggleDisplay(device, flag) {
    if (device.watchdocConnected()) {
        if (flag) {
            // enable
            if (device.hdmi_cec) {
                device.io_watchdog.emit('DISPLAY', 'on');
            }
            if (device.hdmi_legacy) {
                device.io_watchdog.emit('DISPLAY', 'pc-on');
            }
        } else {
            // disable
            if (device.hdmi_cec) {
                device.io_watchdog.emit('DISPLAY', 'off');
            }
            if (device.hdmi_legacy) {
                device.io_watchdog.emit('DISPLAY', 'pc-off');
            }
        }
    } else {
        // Watchdog not (yet) connected!
        req.reportError(device,"Watchdog was NULL while toggle Display", "", 0, 0, "");
    }
}

export function reboot(device) {
    if (device.dependsWatchdog() && device.watchdocConnected()) {
        console.log('ask watchdog to reboot...');
        device.io_watchdog.emit('REBOOT','');
    }
}

export function shutdown(device) {
    if (device.dependsWatchdog() && device.watchdocConnected()) {
        console.log('ask watchdog to shutdown...');
        device.io_watchdog.emit('SHUTDOWN','');
    }
}

export function self_update(device) {
    if (device.dependsWatchdog() && device.watchdocConnected()) {
        console.log('ask watchdog to selfupdate...');
        device.io_watchdog.emit('SELFUPDATE','');
    }
}

export function report_version(device) {
    if (device.dependsWatchdog() && device.watchdocConnected()) {
        console.log('ask watchdog for version...');
        device.io_watchdog.emit('VERSION','');
    }
}