import {reportError} from "./requests";
import * as constants from './constants';
var classifyPoint = require("robust-point-in-polygon")

export async function initRouteMap(gui, device, station) {
    return new Promise(function(resolve, reject)
    {
        try {
            gui.gmap.resetRouteLoadingStates();
            gui.gmap.directionsService = new google.maps.DirectionsService();
            gui.gmap.directionsRenderer = new google.maps.DirectionsRenderer({suppressMarkers: true});
            gui.gmap.trafficLayer = new google.maps.TrafficLayer();
            gui.gmap.route = new google.maps.Map(document.getElementById("route"), {
                zoom: 12,
                center: new google.maps.LatLng(station.lat,station.long),
                disableDefaultUI: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            gui.gmap.route.setOptions({ styles: styles["default"] });
            gui.gmap.trafficLayer.setMap(gui.gmap.route);
            gui.gmap.directionsRenderer.setMap(gui.gmap.route);
            gui.gmap.marker['startroute'] = new google.maps.Marker({
                position: new google.maps.LatLng(station.lat,station.long),
                map: gui.gmap.route,
                icon: {
                    path: constants.SVG_FIRE_STATION,
                    fillColor: "red",
                    fillOpacity: 1,
                    strokeWeight: 0,
                    rotation: 0,
                    scale: 1.25,
                    anchor: new google.maps.Point(12, 12)
                }
            });

            gui.gmap.route.addListener("tilesloaded", () => {
                gui.gmap.routeTilesLoaded = true;
                if (device.printerCapableDevice()) {
                    gui.gmap.triggerRouteScreenshot();
                }
            });

            gui.gmap.route.addListener("zoom_changed", () => {
                updateMarker(gui, gui.gmap.route);
            });


            resolve();
        } catch (e) {
            reportError(device, e.message, 'initRouteMap in gmap.js', 0, 0, e.name);
            reject("failed to initRouteMap");
        }
    });
}

export async function initMissionMap(gui, device, station) {
    return new Promise(function(resolve, reject)
    {
        try {
            gui.gmap.resetMapLoadingStates();
            gui.gmap.map = new google.maps.Map(document.getElementById("map"), {
                zoom: 18,
                center: new google.maps.LatLng(station.lat,station.long),
                disableDefaultUI: true,
                mapTypeId: google.maps.MapTypeId.HYBRID,
                tilt: 0
            });
            gui.gmap.map.setOptions({ styles: styles["default"] });

            gui.gmap.map.addListener("tilesloaded", () => {
                gui.gmap.mapTilesLoaded = true;
                if (device.printerCapableDevice()) {
                    gui.gmap.triggerMapScreenshot();
                }
            });
            gui.gmap.map.addListener("zoom_changed", () => {
                updateMarker(gui, gui.gmap.map);
            });

            if (device.mobileDevice()) {
                gui.gmap.streetviewService = new google.maps.StreetViewService();
            }

            resolve();
        } catch (e) {
            reportError(device, e.message, 'initMissionMap in gmap.js', 0, 0, e.name);
            reject("failed to initMissionMap");
        }
    });
}

function updateMarker(gui, currentMap) {
    const zoom = currentMap.getZoom();
    if (zoom) {
        Object.keys(gui.gmap.marker).forEach(function(key) {
            if (gui.gmap.marker[key].zoom_threshold_changeicon !== undefined && gui.gmap.marker[key].map == currentMap) {
                if (zoom >= gui.gmap.marker[key].zoom_threshold_changeicon) {
                    gui.gmap.marker[key].setIcon(gui.gmap.marker[key].img_big);
                } else {
                    gui.gmap.marker[key].setIcon(gui.gmap.marker[key].img_small);
                }
            }
            if (gui.gmap.marker[key].zoom_threshold_changescale !== undefined && gui.gmap.marker[key].map == currentMap) {
                let curIcon = gui.gmap.marker[key].getIcon();
                if (zoom >= gui.gmap.marker[key].zoom_threshold_changescale) {
                    curIcon.scale = gui.gmap.marker[key].scale_big;
                } else {
                    curIcon.scale = gui.gmap.marker[key].scale_small;
                }
                gui.gmap.marker[key].setIcon(curIcon);
            }
            if (gui.gmap.marker[key].zoom_threshold_hidden !== undefined && gui.gmap.marker[key].recover_map !== undefined && gui.gmap.marker[key].recover_map == currentMap) {
                if (zoom >= gui.gmap.marker[key].zoom_threshold_hidden) {
                    gui.gmap.marker[key].setMap(gui.gmap.marker[key].recover_map);
                } else {
                    gui.gmap.marker[key].setMap(null);
                }
            }
            if (gui.gmap.marker[key].zoom_threshold_visible !== undefined && gui.gmap.marker[key].recover_map !== undefined && gui.gmap.marker[key].recover_map == currentMap) {
                if (zoom >= gui.gmap.marker[key].zoom_threshold_visible) {
                    gui.gmap.marker[key].setMap(gui.gmap.marker[key].recover_map);
                } else {
                    gui.gmap.marker[key].setMap(null);
                }
            }

        });
    }
}

function drawCircleOutlineInMap(gui, name, radius, color, strokeWidth, bindMap, bindMarker) {
    gui.gmap.marker[name] = new google.maps.Circle({
        map: bindMap,
        radius: radius,
        fillOpacity: 0,
        strokeColor: color,
        strokeOpacity: 0.6,
        strokeWeight: strokeWidth
    });
    gui.gmap.marker[name].bindTo('center', bindMarker,'position');
}

export function drawPointOfMission(gui, device, alarm) {
    try {
        console.log(" ~~~> Prepare Einsatzort...", alarm.depesche);
        let pomCoord = new google.maps.LatLng(alarm.lat(),alarm.long());
        gui.gmap.map.setCenter(pomCoord);
        gui.gmap.marker['einsatz'] = new google.maps.Marker({
            position: pomCoord,
            map: gui.gmap.map,
            icon: {
                path: constants.SVG_FIRE_SITE,
                fillColor: "red",
                fillOpacity: 1,
                rotation: 0,
                strokeWeight: 0,
                scale: 1.25,
                anchor: new google.maps.Point(12, 12)
            }
        });

        gui.gmap.info['einsatz'] = new google.maps.InfoWindow({
            content: "<b>"+alarm.depesche.Strasse+"<br />"+alarm.depesche.Ort+"</b><br /><a href=\""+alarm.navigationUrl(device)+"\" target=\"_blank\" style=\"padding: 6px; display: block; background-color: red; color: white;\">Navigation starten!</a>"
        });
        gui.gmap.info['endroute'] = new google.maps.InfoWindow({
            content: "<b>"+alarm.depesche.Strasse+"<br />"+alarm.depesche.Ort+"</b><br /><a href=\""+alarm.navigationUrl(device)+"\" target=\"_blank\" style=\"padding: 6px; display: block; background-color: red; color: white;\">Navigation starten!</a>"
        });
        gui.gmap.marker['einsatz'].addListener("click", () => {
            gui.gmap.info['einsatz'].open({map: gui.gmap.map,anchor: gui.gmap.marker['einsatz'],shouldFocus: false });
        });
        gui.gmap.marker['endroute'] = new google.maps.Marker({
            position: new google.maps.LatLng(alarm.lat(),alarm.long()),
            map: gui.gmap.route,
            icon: {
                path: constants.SVG_GPS_DESTINATION,
                fillColor: "black",
                fillOpacity: 1,
                strokeWeight: 1,
                strokeColor: "white",
                strokeOpacity: 1,
                rotation: 0,
                scale: 1.25,
                anchor: new google.maps.Point(12, 12)
            }
        });
        gui.gmap.marker['endroute'].addListener("click", () => {
            gui.gmap.info['endroute'].open({map: gui.gmap.route,anchor: gui.gmap.marker['endroute'],shouldFocus: false });
        });

        drawCircleOutlineInMap(gui, 'einsatz-circle-inner', 50, "red", 3, gui.gmap.map, gui.gmap.marker['einsatz']);
        drawCircleOutlineInMap(gui, 'einsatz-circle-outer', 100, "green", 3, gui.gmap.map, gui.gmap.marker['einsatz']);

        drawCircleOutlineInMap(gui, 'route-circle-inner', 50, "red", 1, gui.gmap.route, gui.gmap.marker['endroute']);
        drawCircleOutlineInMap(gui, 'route-circle-outer', 100, "green", 1, gui.gmap.route, gui.gmap.marker['endroute']);

        if (device.mobileDevice()) {
            console.log("getPanorama init");
            if (gui.gmap.streetviewService) {
                console.log("getPanorama call", gui.gmap.streetviewService);
                gui.gmap.streetviewService.getPanorama(
                    {
                        location: pomCoord
                    },
                    function(data, status) {
                        if (status == google.maps.StreetViewStatus.OK) {
                            console.log("getPanorama OK", data.location.pano);
                            gui.gmap.mapPanorama = gui.gmap.map.getStreetView();
                            gui.gmap.mapPanorama.setOptions({
                                enableCloseButton: true,
                                addressControl: false,
                                zoom: 0
                            });

                            gui.gmap.mapPanorama.addListener("visible_changed", () => {
                                console.log("panoi visible", gui.gmap.mapPanorama.getVisible());
                                if (gui.gmap.mapPanorama.getVisible()) {
                                    gui.gmap.marker['wind'].setMap(null);
                                } else {
                                    gui.gmap.marker['wind'].setMap(gui.gmap.map);
                                }
                            });
                            gui.gmap.mapPanorama.setPano(data.location.pano);
                            gui.gmap.mapPanorama.setPov({
                                heading: google.maps.geometry.spherical.computeHeading(data.location.latLng, pomCoord),
                                pitch: 0
                            });
                            gui.gmap.map.controls[google.maps.ControlPosition.TOP_CENTER].push(createStreetviewButton(gui));
                        } else {
                            console.log("getPanorama no pano found");
                        }
                    }
                );
            }
        } else {
            console.log("init Streetview skipped because no mobile");
        }

        console.log("POM done");
    } catch (e) {
        reportError(device, e.message, 'drawPointOfMission in gmap.js', 0, 0, e.name);
        console.log("failed to drawPointOfMission");
    }
}

function createStreetviewButton(gui, pomCoord) {
    const div = document.createElement("div");
    const controlButton = document.createElement("button");

    // Set CSS for the control.
    controlButton.style.backgroundColor = "#fff";
    controlButton.style.border = "2px solid #fff";
    controlButton.style.borderRadius = "3px";
    controlButton.style.boxShadow = "0 2px 6px rgba(0,0,0,.3)";
    controlButton.style.color = "rgb(25,25,25)";
    controlButton.style.cursor = "pointer";
    controlButton.style.fontFamily = "Roboto,Arial,sans-serif";
    controlButton.style.fontSize = "16px";
    controlButton.style.lineHeight = "38px";
    controlButton.style.margin = "8px 0 22px";
    controlButton.style.padding = "0 5px";
    controlButton.style.textAlign = "center";
    controlButton.textContent = "360° Ansicht öffnen";
    controlButton.title = "360° Streetview Ansicht starten";
    controlButton.type = "button";
    // Setup the click event listeners: simply set the map to Chicago.
    controlButton.addEventListener("click", () => {
        const toggle = gui.gmap.mapPanorama.getVisible();
        if (toggle == false) {
            gui.gmap.mapPanorama.setVisible(true);
        } else {
            gui.gmap.mapPanorama.setVisible(false);
        }
    });
    div.appendChild(controlButton)
    return div;
}

export function drawHydrants(gui, device, alarm, hydrants) {
    console.log(hydrants);
    let marker_helper = new google.maps.Marker({
        position: new google.maps.LatLng(alarm.lat(),alarm.long())
    });
    let hydrant_types = [];
    if (hydrants.hydrant_types != null && hydrants.hydrant_types.length > 0) {
        for (let i = 0; i < hydrants.hydrant_types.length; i++) {
            if (hydrants.hydrant_types[i].svg_tactical.length > 0 && hydrants.hydrant_types[i].svg_overview.length > 0) {
                hydrant_types.push({
                    id: hydrants.hydrant_types[i].id,
                    name: hydrants.hydrant_types[i].name,
                    marker_mission: {
                        path: hydrants.hydrant_types[i].svg_tactical,
                        fillColor: "yellow",
                        fillOpacity: 0.75,
                        strokeWeight: 0,
                        rotation: 0,
                        scale: 1.25,
                        anchor: new google.maps.Point(17, 17)
                    },
                    marker_route: {
                        path: hydrants.hydrant_types[i].svg_overview,
                        fillColor: "red",
                        fillOpacity: 1,
                        strokeWeight: 0,
                        rotation: 0,
                        scale: 0.75,
                        anchor: new google.maps.Point(17, 17)
                    }
                });
            } else {
                hydrant_types.push({
                    id: hydrants.hydrant_types[i].id,
                    name: hydrants.hydrant_types[i].name,
                    marker_mission: hydrants.hydrant_types[i].icon,
                    marker_route: null
                });
            }
        }
    }

    if (hydrants.hydrants != null && hydrants.hydrants.length > 0) {
        let htype = null;
        for (let i = 0; i < hydrants.hydrants.length; i++) {
            htype = hydrant_types.find(obj => {
                return obj.id === hydrants.hydrants[i].hydranttype_id;
            });
            if (htype == null) {
                continue;
            }
            gui.gmap.marker['hy-'+hydrants.hydrants[i]['id']] = new google.maps.Marker({
                position: new google.maps.LatLng(hydrants.hydrants[i]['lat'],hydrants.hydrants[i]['long']),
                map: gui.gmap.map,
                icon: htype.marker_mission,
                zIndex: 99,
                scale_small: 0.75,
                scale_big: 1.25,
                zoom_threshold_changescale: 16
            });
            //let distance = haversine_distance(gui.gmap.marker['hy-'+hydrants.hydrants[i]['id']], marker_helper) * 1000; // we use meters
            let distance = hydrants.hydrants[i]['distance'];
            gui.gmap.info['hy-'+hydrants.hydrants[i]['id']] = new google.maps.InfoWindow({
                content: "<b>"+hydrants.hydrants[i]['name']+" (DN "+ hydrants.hydrants[i]['flowrate'] + ")</b><br />"+htype.name+"<br />Entfernung: " + distance.toFixed(0) + "m<br />ca. <b>"+Math.ceil(distance / 20.0 * 1.2 )+" B-Längen</b>"
            });
            gui.gmap.marker['hy-'+hydrants.hydrants[i]['id']].addListener("click", () => {
                gui.gmap.info['hy-'+hydrants.hydrants[i]['id']].open(gui.gmap.map, gui.gmap.marker['hy-'+hydrants.hydrants[i]['id']]);
            });
            if (htype.marker_route != null) {
                gui.gmap.marker['hy-route-'+hydrants.hydrants[i]['id']] = new google.maps.Marker({
                    position: new google.maps.LatLng(hydrants.hydrants[i]['lat'],hydrants.hydrants[i]['long']),
                    map: gui.gmap.route,
                    icon: htype.marker_route,
                    zIndex: 99
                });
            }
        }
    }

    gui.gmap.mapMarkerLoaded = true;
    if (device.printerCapableDevice()) {
        gui.gmap.triggerMapScreenshot();
    }
    gui.gmap.routeMarkerLoaded = true;
    if (device.printerCapableDevice()) {
        gui.gmap.triggerRouteScreenshot();
    }
}

export function drawAssemblyAreas(gui, device, alarm, areas) {
    if (areas.length > 0) {
        for (let i = 0; i < areas.length; i++) {
            gui.gmap.marker['br-mark-'+areas[i]['id']] = new google.maps.Marker({
                position: new google.maps.LatLng(areas[i]['lat'],areas[i]['long']),
                map: gui.gmap.route,
                icon: areas[i]['img_small'],
                zIndex: 99,
                zoom_threshold_changeicon: 17,
                img_big: areas[i]['img_big'],
                img_small: areas[i]['img_small']
            });

            gui.gmap.info['br-mark-'+areas[i]['id']] = new google.maps.InfoWindow({
                content: "<b>Bereitstellungsraum " + areas[i]['label'] + "</b>"
            });
            gui.gmap.marker['br-mark-'+areas[i]['id']].addListener("click", () => {
                gui.gmap.info['br-mark-'+areas[i]['id']].open(gui.gmap.route, gui.gmap.marker['br-mark-'+areas[i]['id']]);
            });

            let coords = null;
            try {
                coords = JSON.parse(areas[i]['coords']);
            } catch (e) {
                console.log(e);
            }
            if (coords != null) {
                gui.gmap.marker['br-poly-' + areas[i]['id']] = new google.maps.Polygon({
                    paths: coords,
                    strokeColor: "#ff0000",
                    strokeOpacity: 0.75,
                    strokeWeight: 2,
                    fillColor: "#0000ff",
                    fillOpacity: 0.25,
                    map: gui.gmap.route,
                    zIndex: 98,
                    zoom_threshold_visible: 17,
                    recover_map: gui.gmap.route
                });
            }
        }
    }
}

export function drawRescuePoints(gui, device, alarm, rps) {
    if (rps.length > 0) {
        for (let i = 0; i < rps.length; i++) {
            gui.gmap.marker['rp-'+rps[i]['id']] = new google.maps.Marker({
                position: new google.maps.LatLng(rps[i]['lat'],rps[i]['long']),
                map: gui.gmap.map,
                icon: rps[i]['img_big'],
                zoom_threshold_changeicon: 18,
                img_big: rps[i]['img_big'],
                img_small: rps[i]['img_small'],
                zIndex: 99
            });

            gui.gmap.info['rp-'+rps[i]['id']] = new google.maps.InfoWindow({
                content: "<b>"+rps[i]['type_readable']+"</b><br />"+rps[i]['name']
            });
            gui.gmap.marker['rp-'+rps[i]['id']].addListener("click", () => {
                gui.gmap.info['rp-'+rps[i]['id']].open(gui.gmap.map, gui.gmap.marker['rp-'+rps[i]['id']]);
            });
        }
    }
}

export async function calcRoute(gui, device, station, alarm) {
    return new Promise(function(resolve, reject)
    {
        try {
            console.log(" ~~~> Calculate and Display Route...");
            let origin = null;
            if (alarm.station !== null) {
                origin = new google.maps.LatLng(alarm.station.lat,alarm.station.long);
            } else {
                origin = new google.maps.LatLng(station.lat,station.long);
            }

            gui.gmap.directionsService.route(
                {
                    origin: origin,
                    destination: new google.maps.LatLng(alarm.lat(),alarm.long()),
                    travelMode: "DRIVING",
                    drivingOptions: {
                        departureTime: new Date(),
                        trafficModel: 'optimistic'
                    },
                    unitSystem: google.maps.UnitSystem.METRIC
                },
                function (response, status) {
                    if (status == "OK") {
                        gui.gmap.directionsRenderer.setDirections(response);
                        console.debug(response);
                        let route = response.routes[0];
                        let distance = 0.0;
                        let duration = 0.0;
                        // For each route, display summary information.
                        for (let i = 0; i < route.legs.length; i++) {
                            duration += route.legs[i].duration.value;
                            distance += route.legs[i].distance.value;
                        }
                        alarm.duration = duration;
                        alarm.distance = distance;

                        gui.updateMissionDistanceInformation(alarm);

                        // Set max zoom level if map is zoomed in too much
                        const MAX_ZOOM = 18; // Definiere einen maximalen Zoomlevel
                        google.maps.event.addListenerOnce(gui.gmap.route, 'bounds_changed', function() {
                            if (gui.gmap.route.getZoom() > MAX_ZOOM) {
                                console.log("Map bounds adjusted");
                                gui.gmap.route.setZoom(MAX_ZOOM); // Maximaler Zoom setzen
                            }
                        });

                        gui.gmap.routeDirectionLoaded = true;
                        if (device.printerCapableDevice()) {
                            gui.gmap.triggerRouteScreenshot();
                        }

                        resolve();
                    } else {
                        reportError(device, "calcRoute Failed", 'calcuRoute in gmap.js failed with ' + status, 0, 0, null);
                        reject("Directions request failed due to " + status);
                    }
                }
            );
        } catch (e) {
            reportError(device, e.message, 'calcuRoute in gmap.js', 0, 0, e.name);
            reject("failed to calcuRoute");
        }
    });
}

export async function findPanorama(gui, device, station, alarm) {
    return new Promise(function(resolve, reject)
    {
        try {
            console.log(" ~~~> Calculate and Display Route...");

            gui.gmap.directionsService.route(
                {
                    origin: new google.maps.LatLng(station.lat,station.long),
                    destination: new google.maps.LatLng(alarm.lat(),alarm.long()),
                    travelMode: "DRIVING",
                    drivingOptions: {
                        departureTime: new Date(),
                        trafficModel: 'optimistic'
                    },
                    unitSystem: google.maps.UnitSystem.METRIC
                },
                function (response, status) {
                    if (status == "OK") {
                        gui.gmap.directionsRenderer.setDirections(response);
                        console.debug(response);
                        let route = response.routes[0];
                        let distance = 0.0;
                        let duration = 0.0;
                        // For each route, display summary information.
                        for (let i = 0; i < route.legs.length; i++) {
                            duration += route.legs[i].duration.value;
                            distance += route.legs[i].distance.value;
                        }
                        alarm.duration = duration;
                        alarm.distance = distance;

                        gui.gmap.routeDirectionLoaded = true;
                        if (device.printerCapableDevice()) {
                            gui.gmap.triggerRouteScreenshot();
                        }

                        resolve();
                    } else {
                        reportError(device, "Directions request failed", 'Directions request failed with ' + status, 0, 0, null);
                        reject("Directions request failed due to " + status);
                    }
                }
            );
        } catch (e) {
            reportError(device, e.message, 'calcuRoute in gmap.js', 0, 0, e.name);
            reject("failed to calcuRoute");
        }
    });
}

export function drawWindDirection(gui, device, alarm) {
    if (alarm.weather !== null) {
        let degree = alarm.weather.WindDirectionDegrees;
        // images and degree from api differ in 180 degree and therefore needs to be swapped (0 => 180, 90 => 270, 180 => 0, 270 => 90)
        let img = (Math.round(degree / 10) * 10) + 180;
        if (img >= 360) {
            img -= 360;
        }

        let oImg = new Image(); // we to this to preload image to have it for print
        oImg.src = gui.assets_dir + '/weather/wind-direction/' + img + '.png';

        let wind_image = {
            url: oImg.src,
            size: new google.maps.Size(400, 400),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(200, 200),
            scaledSize: new google.maps.Size(400, 400)
        };


        gui.gmap.marker['wind'] = new google.maps.Marker({
            position: new google.maps.LatLng(alarm.lat(), alarm.long()),
            map: gui.gmap.map,
            icon: wind_image,
            zIndex: -1
        });

    }
    gui.gmap.mapWindLoaded = true;
    if (device.printerCapableDevice()) {
        gui.gmap.triggerMapScreenshot();
    }
}


export function drawEinsatzobjekte(gui, device, alarm, objekte) {
    if (objekte.length > 0) {
        for (let i = 0; i < objekte.length; i++) {
            var polygon = [];

            // Distance to alert pom
            objekte[i]['distance'] = gui.gmap.haversine_distance_raw(alarm.depesche.Lat, alarm.depesche.Long, objekte[i]['lat'], objekte[i]['long']) * 1000;

            if (objekte[i]['coords'] != null) {
                for (let j = 0; j < objekte[i]['coords'].length; j++) {
                    polygon.push([objekte[i]['coords'][j]['lat'], objekte[i]['coords'][j]['lng']]);
                }
            }
            if (polygon.length == 0 || classifyPoint(polygon, [alarm.depesche.Lat, alarm.depesche.Long]) > 0) {
                // Point not in Polygon
                let markerIndex = 'objekt-mark-' + objekte[i]['id'];
                gui.gmap.marker[markerIndex] = new google.maps.Marker({
                    position: new google.maps.LatLng(objekte[i]['lat'], objekte[i]['long']),
                    map: gui.gmap.map,
                    icon: {
                        path: "M12,2C15.31,2 18,4.66 18,7.95C18,12.41 12,19 12,19C12,19 6,12.41 6,7.95C6,4.66 8.69,2 12,2M12,6A2,2 0 0,0 10,8A2,2 0 0,0 12,10A2,2 0 0,0 14,8A2,2 0 0,0 12,6M20,19C20,21.21 16.42,23 12,23C7.58,23 4,21.21 4,19C4,17.71 5.22,16.56 7.11,15.83L7.75,16.74C6.67,17.19 6,17.81 6,18.5C6,19.88 8.69,21 12,21C15.31,21 18,19.88 18,18.5C18,17.81 17.33,17.19 16.25,16.74L16.89,15.83C18.78,16.56 20,17.71 20,19Z",
                        fillColor: "red",
                        fillOpacity: 1,
                        strokeWeight: 0,
                        rotation: 0,
                        scale: 1.25,
                        anchor: new google.maps.Point(17, 17)
                    },
                    zIndex: 99
                });
                gui.gmap.info[markerIndex] = new google.maps.InfoWindow({
                    content: "<b>"+objekte[i]['label']+"</b>"
                });
                gui.gmap.marker[markerIndex].addListener("click", () => {
                    gui.gmap.info[markerIndex].open(gui.gmap.map, gui.gmap.marker[markerIndex]);
                });
                alarm.object_docu_nearby.push(objekte[i]);
            } else {
                // Point in Polygon
                // Update POM Markers
                gui.gmap.info['einsatz'] = new google.maps.InfoWindow({
                    content: "<b>"+objekte[i]['label']+"<br />"+alarm.depesche.Strasse+"<br />"+alarm.depesche.Ort+"</b><br /><a href=\"https://www.google.com/maps?saddr=My+Location&daddr="+alarm.lat()+","+alarm.long()+"\" target=\"_blank\" style=\"padding: 6px; display: block; background-color: red; color: white;\">Navigation starten!</a>"
                });
                gui.gmap.info['endroute'] = new google.maps.InfoWindow({
                    content: "<b>"+objekte[i]['label']+"<br />"+alarm.depesche.Strasse+"<br />"+alarm.depesche.Ort+"</b><br /><a href=\"https://www.google.com/maps?saddr=My+Location&daddr="+alarm.lat()+","+alarm.long()+"\" target=\"_blank\" style=\"padding: 6px; display: block; background-color: red; color: white;\">Navigation starten!</a>"
                });
                // delegate the objekt
                if (alarm.einsatzobjekt == null) {
                    // delegate any object
                    alarm.einsatzobjekt = objekte[i];
                } else {
                    // if more objects fit into polygon only overwrite if object if from own station
                    if (objekte[i].station_id == device.station_id) {
                        alarm.object_docu_nearby.push(alarm.einsatzobjekt); // removed item goes to nearby items
                        alarm.einsatzobjekt = objekte[i];
                    }
                }
            }

            if (objekte[i]['piktos'].length > 0) {
                for (let j = 0; j < objekte[i]['piktos'].length; j++) {
                    let piktoIndex = 'objekt-pikto-'+objekte[i]['piktos'][j]['id'];
                    gui.gmap.marker[piktoIndex] = new google.maps.Marker({
                        position: new google.maps.LatLng(objekte[i]['piktos'][j]['lat'],objekte[i]['piktos'][j]['long']),
                        map: gui.gmap.map,
                        icon: objekte[i]['piktos'][j]['icon'],
                        zIndex: 99,
                        zoom_threshold_hidden: 16,
                        recover_map: gui.gmap.map
                    });

                    gui.gmap.info[piktoIndex] = new google.maps.InfoWindow({
                        content: "<b>"+objekte[i]['piktos'][j]['label']+"</b>"
                    });
                    gui.gmap.marker[piktoIndex].addListener("click", () => {
                        gui.gmap.info[piktoIndex].open(gui.gmap.map, gui.gmap.marker[piktoIndex]);
                    });
                }
            }

        }
    }
}

const styles = {
    default: [
        {
            featureType: "poi",
            stylers: [{ visibility: "off" }],
        },
        {
            featureType: "administrative",
            stylers: [{ visibility: "on" }],
        },
        {
            featureType: "transit",
            stylers: [{ visibility: "off" }],
        },
        {
            featureType: "road",
            stylers: [{ visibility: "on" }],
        },
        {
            featureType: "landscape",
            stylers: [{ visibility: "on" }],
        },
        {
            featureType: "water",
            stylers: [{ visibility: "on" }],
        },
    ],
    night: [
        { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
        { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
        { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
        {
            featureType: "administrative.locality",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
        },
        {
            featureType: "poi",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
        },
        {
            featureType: "poi.park",
            elementType: "geometry",
            stylers: [{ color: "#263c3f" }],
        },
        {
            featureType: "poi.park",
            elementType: "labels.text.fill",
            stylers: [{ color: "#6b9a76" }],
        },
        {
            featureType: "road",
            elementType: "geometry",
            stylers: [{ color: "#38414e" }],
        },
        {
            featureType: "road",
            elementType: "geometry.stroke",
            stylers: [{ color: "#212a37" }],
        },
        {
            featureType: "road",
            elementType: "labels.text.fill",
            stylers: [{ color: "#9ca5b3" }],
        },
        {
            featureType: "road.highway",
            elementType: "geometry",
            stylers: [{ color: "#746855" }],
        },
        {
            featureType: "road.highway",
            elementType: "geometry.stroke",
            stylers: [{ color: "#1f2835" }],
        },
        {
            featureType: "road.highway",
            elementType: "labels.text.fill",
            stylers: [{ color: "#f3d19c" }],
        },
        {
            featureType: "transit",
            elementType: "geometry",
            stylers: [{ color: "#2f3948" }],
        },
        {
            featureType: "transit.station",
            elementType: "labels.text.fill",
            stylers: [{ color: "#d59563" }],
        },
        {
            featureType: "water",
            elementType: "geometry",
            stylers: [{ color: "#17263c" }],
        },
        {
            featureType: "water",
            elementType: "labels.text.fill",
            stylers: [{ color: "#515c6d" }],
        },
        {
            featureType: "water",
            elementType: "labels.text.stroke",
            stylers: [{ color: "#17263c" }],
        },
    ],
};