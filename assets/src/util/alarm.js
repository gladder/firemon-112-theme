import * as constants from '../util/constants';
import * as gmapflow from '../util/gmap';
import * as req from "./requests";
import {reportError} from "./requests";

export function enableAlarmDepricated(alarm, gui, device, station) {
    console.log("ALARM", alarm);
    try {

        if (alarm.depesche.MitalarmierteKraefte != null && alarm.depesche.MitalarmierteKraefte.length > 0) {
            alarm.depesche.MitalarmierteKraefte = alarm.depesche.MitalarmierteKraefte.map(item => {
                return station.makReplace(item);
            })
        }

        gui.state = constants.GUI_STATE_ALERT;
        device.toggleDisplay(true);
        gui.hideIdle();

        gui.displayDepesche(alarm, station, device);

        gmapflow.initRouteMap(gui, device, station).then(
            function () {
                console.log("routeMap done");
                gmapflow.calcRoute(gui, device, station, alarm).then(
                    function () {
                        console.log("calcRoute done");
                        gmapflow.initMissionMap(gui, device, station).then(
                            function () {
                                console.log("missionMap done");
                                gmapflow.drawPointOfMission(gui, device, alarm);

                                gui.displayWeather(alarm);
                                gmapflow.drawWindDirection(gui, device, alarm);
                                console.log("weather drawn");

                                req.getEinsatzobjekteNearby(device, alarm).then(
                                    function (objekte) {
                                        gmapflow.drawEinsatzobjekte(gui, device, alarm, objekte);
                                        gui.displayEinsatzobjektDetails(alarm, device);
                                        console.log("drawEinsatzobjekte done");

                                        req.getHydrantsNearby(device, alarm).then(
                                            function (hydrants) {
                                                gmapflow.drawHydrants(gui, device, alarm, hydrants);
                                                console.log("drawHydrants done");
                                                req.getAssemblyAreasNearby(device, alarm).then(
                                                    function(areas) {
                                                        gmapflow.drawAssemblyAreas(gui, device, alarm, areas);
                                                        console.log("drawAssemblyAreas done");
                                                        req.getRescuePointsNearby(device, alarm).then(
                                                            function(rps) {
                                                                gmapflow.drawRescuePoints(gui, device, alarm, rps);
                                                                console.log("drawRescuePoints done");
                                                                jQuery(document).trigger(constants.EVT_ALERT_GUI_RENDERING_FINISHED);
                                                            },
                                                            function (err) {
                                                                console.log(err);
                                                                handleAlarmErr(new Error("getRescuePointsNearby failed!"), device);
                                                            }
                                                        );
                                                    },
                                                    function(err) {
                                                        console.log(err);
                                                        handleAlarmErr(new Error("getAssembyAreasNearby failed!"), device);
                                                    }
                                                );
                                            },
                                            function (err) {
                                                console.log(err);
                                                handleAlarmErr(new Error("getHydrantsNearby failed!"), device);
                                            }
                                        );
                                    },
                                    function (err) {
                                        console.log(err);
                                        handleAlarmErr(new Error("getEinsatzobjekteNearby failed!"), device);
                                    }
                                );
                            },
                            function (err) {
                                console.log(err);
                                handleAlarmErr(new Error("initMissionMap failed!"), device);
                            }
                        );
                    },
                    function (err) {
                        console.log(err);
                        handleAlarmErr(new Error("calcRoute failed!"), device);
                    }
                );
            },
            function (err) {
                console.log(err);
                handleAlarmErr(new Error("initRouteMap failed!"), device);
            }
        );

        // Speech
        if (device.speech_enabled && device.speech_count > 0) {
            req.synthDepesche(device, alarm, station).then(
                function (speechMp3) {
                    Promise.all([req.loadAudio(device, gui.depescheGongSrc(device)), req.loadAudio(device, speechMp3)]).then(values => {
                        gui.gongAudio = values[0];
                        alarm.speechAudio = values[1];
                        setTimeout(function () {
                            jQuery(document).trigger(constants.EVT_DEPESCHE_SPEECH);
                        }, device.speech_delay * 1000);
                    }, reason => {
                        console.log(reason)
                        handleAlarmErr(new Error("Promises of synthDepesche failed!"), device);
                    });
                },
                function (err) {
                    console.log(err);
                    handleAlarmErr(new Error("synthDepesche failed!"), device);
                }
            );
        }

        // reset pending disableAlarm in case of new alarm while displaying alarm
        if (alarm.disable_timeout_id !== null) {
            clearTimeout(alarm.disable_timeout_id);
            alarm.disable_timeout_id = null;
            console.log('disable timeout cleared');
        }
        alarm.disable_timeout_id = setTimeout(disableAlarm, device.alarm_timeout, false, alarm, gui, device);
    } catch (e) {
        handleAlarmErr(e, device);
    }
}

export async function enableAlarm(alarm, gui, device, station) {
    try {
        // 1. Preprocess replacement of personnel strings.
        if (alarm.depesche?.MitalarmierteKraefte?.length > 0) {
            alarm.depesche.MitalarmierteKraefte = alarm.depesche.MitalarmierteKraefte.map(item =>
                station.makReplace(item)
            );
        }

        // 2. Update GUI state.
        gui.state = constants.GUI_STATE_ALERT;
        device.toggleDisplay(true);
        gui.hideIdle();
        gui.displayDepesche(alarm, station, device);

        // 3. Map initialization chain (dependent tasks)
        try {
            await gmapflow.initRouteMap(gui, device, station);
            console.log("initRouteMap done");
        } catch (err) {
            console.log(err);
            handleAlarmErr(new Error("initRouteMap failed!"), device);
        }

        try {
            await gmapflow.calcRoute(gui, device, station, alarm);
            console.log("calcRoute done");
        } catch (err) {
            console.log(err);
            handleAlarmErr(new Error("calcRoute failed!"), device, false);
        }

        try {
            await gmapflow.initMissionMap(gui, device, station);
            console.log("initMissionMap done");
        } catch (err) {
            console.log(err);
            handleAlarmErr(new Error("initMissionMap failed!"), device);
        }

        // 4. Draw mission point and weather (dependent on maps)
        try {
            gmapflow.drawPointOfMission(gui, device, alarm);
            gui.displayWeather(alarm);
            gmapflow.drawWindDirection(gui, device, alarm);
            console.log("Point of mission, weather, and wind drawn");
        } catch (err) {
            console.log(err);
            handleAlarmErr(new Error("Drawing mission or weather failed!"), device);
        }

        // 5. Fetch nearby objects in parallel since they are independent.
        const nearbyPromises = [
            req.getEinsatzobjekteNearby(device, alarm).catch(err => {
                console.log("getEinsatzobjekteNearby failed", err);
                handleAlarmErr(new Error("getEinsatzobjekteNearby failed!"), device, false);
                return null;
            }),
            req.getHydrantsNearby(device, alarm).catch(err => {
                console.log("getHydrantsNearby failed", err);
                handleAlarmErr(new Error("getHydrantsNearby failed!"), device);
                return null;
            }),
            req.getAssemblyAreasNearby(device, alarm).catch(err => {
                console.log("getAssemblyAreasNearby failed", err);
                handleAlarmErr(new Error("getAssemblyAreasNearby failed!"), device, false);
                return null;
            }),
            req.getRescuePointsNearby(device, alarm).catch(err => {
                console.log("getRescuePointsNearby failed", err);
                handleAlarmErr(new Error("getRescuePointsNearby failed!"), device, false);
                return null;
            })
        ];
        const [einsatzObjekte, hydrants, assemblyAreas, rescuePoints] = await Promise.all(nearbyPromises);

        if (einsatzObjekte) {
            try {
                gmapflow.drawEinsatzobjekte(gui, device, alarm, einsatzObjekte);
                gui.displayEinsatzobjektDetails(alarm, device);
                console.log("drawEinsatzobjekte done");
            } catch (err) {
                console.log(err);
                handleAlarmErr(new Error("Drawing Einsatzobjekte failed!"), device);
            }
        }
        if (hydrants) {
            try {
                gmapflow.drawHydrants(gui, device, alarm, hydrants);
                console.log("drawHydrants done");
            } catch (err) {
                console.log(err);
                handleAlarmErr(new Error("Drawing Hydrants failed!"), device);
            }
        }
        if (assemblyAreas) {
            try {
                gmapflow.drawAssemblyAreas(gui, device, alarm, assemblyAreas);
                console.log("drawAssemblyAreas done");
            } catch (err) {
                console.log(err);
                handleAlarmErr(new Error("Drawing Assembly Areas failed!"), device);
            }
        }
        if (rescuePoints) {
            try {
                gmapflow.drawRescuePoints(gui, device, alarm, rescuePoints);
                console.log("drawRescuePoints done");
            } catch (err) {
                console.log(err);
                handleAlarmErr(new Error("Drawing Rescue Points failed!"), device);
            }
        }

        // Signal that alert GUI rendering is finished.
        jQuery(document).trigger(constants.EVT_ALERT_GUI_RENDERING_FINISHED);

        // 6. Handle speech independently.
        if (device.speech_enabled && device.speech_count > 0) {
            req.synthDepesche(device, alarm, station)
                .then(speechMp3 => {
                    Promise.all([
                        req.loadAudio(device, gui.depescheGongSrc(device)),
                        req.loadAudio(device, speechMp3)
                    ])
                        .then(values => {
                            gui.gongAudio = values[0];
                            alarm.speechAudio = values[1];
                            setTimeout(() => {
                                jQuery(document).trigger(constants.EVT_DEPESCHE_SPEECH);
                            }, device.speech_delay * 1000);
                        })
                        .catch(reason => {
                            console.log(reason);
                            handleAlarmErr(new Error("Loading speech audio failed!"), device, false);
                        });
                })
                .catch(err => {
                    console.log(err);
                    handleAlarmErr(new Error("synthDepesche failed!"), device, false);
                });
        }

        // 7. Reset disableAlarm timer.
        if (alarm.disable_timeout_id !== null) {
            clearTimeout(alarm.disable_timeout_id);
            alarm.disable_timeout_id = null;
            console.log("disable timeout cleared");
        }
        alarm.disable_timeout_id = setTimeout(disableAlarm, device.alarm_timeout, false, alarm, gui, device);
    } catch (e) {
        handleAlarmErr(e, device);
    }
}


function handleAlarmErr(e, device, reloadAlert = true) {
    reportError(device, e.message, 'enableAlarm FAILED!', 0, 0, "");
    if (reloadAlert) {
        console.warn("Alert will be Reloaded due to ERROR");
        // retry in 20s...
        setTimeout(function(){
            jQuery(document).trigger(constants.EVT_CHECK_ALERT);
        }, 20000);
    } else {
        console.warn("Alert will not be Reloaded since Error was not critical");
    }
}

export function disableAlarm(newAlertFollowing, alarm, gui, device) {
    console.log('disable alert');

    gui.state = constants.GUI_STATE_IDLE;
    device.toggleDisplay(false);
    gui.idle_dialog.show();

    jQuery(document).trigger(constants.EVT_RESET_ALERT);
    gui.displayDepesche(null, null, null);
}