import * as constants from '../util/constants';

/**
 * @param gui
 * @param alarm
 */
export default function clock(gui, alarm) {
    if (gui === null) {
        return;
    }
    gui.seconds_running++;
    let now = new Date();
    let timeStr = now.getHours() + ":" + gui.pad(now.getMinutes(), 2) + ":" + gui.pad(now.getSeconds(), 2);
    if (gui.monitor_clock !== null && gui.state == constants.GUI_STATE_ALERT) {
        if (alarm.einsatzbeginn() !== null) {
            let duration = (now - alarm.einsatzbeginn()) / 1000;
            let minutes = Math.floor(duration / 60);
            gui.monitor_clock.html('<h4><i class="f112-icon f112-hourglass"></i> ' + minutes + "m " + Math.round(duration - (minutes*60)) + "s</h4>");
        } else {
            gui.monitor_clock.html(now.getDate() + "." + now.getMonth() + "." + now.getFullYear() + " " + timeStr);
        }
    }
    if (gui.idle_dialog !== null && gui.state == constants.GUI_STATE_IDLE) {
        gui.idle_dialog.find('.idle-clock').html("<h1>" + timeStr + "</h1>");
    }
    if (gui.seconds_running % 30 == 0) {
        gui.updateMissionDistanceInformation(alarm);
    }
}

