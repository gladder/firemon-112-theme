import { reportError } from '../util/requests';

export default class Mq {
    constructor() {
        this.id = 0;
        this.username =  "";
        this.password = "";
        this.host = "";
        this.port = 0;
        this.vhost = "";
        this.timeout = 10;
        this.client = null;
        this.use_ssl = false;
    }

    update(data) {
        try {
            this.id = data.id;
            this.username = data.user;
            this.password = data.pass;
            this.host = data.host;
            this.port = data.port;
            this.vhost = data.vhost;
            this.use_ssl = data.use_ssl;
        } catch (e) {
            reportError(this, e.message, 'update in Mq', 0, 0, e.name);
        }
    }

    ready() {
        return this.id == 0 || (this.id > 0 && this.connected());
    }

    connected() {
        return this.client != null && this.client.isConnected();
    }
}