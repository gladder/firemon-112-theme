import * as constants from '../util/constants';
import Gmap from '../classes/Gmap';
import * as req from "../util/requests";
import {
    createBold,
    createDiv, createH1,
    createH3, createH4, createI,
    createImage,
    createLi,
    createLink,
    createParagraph, createSpan,
    createUl
} from "../util/htmlgen";

export default class Gui {
    constructor() {
        this.assets_dir = $('body').attr('data-assets');
        this.monitor_clock = $('#monitor-clock');
        this.monitor_ident = $('#monitor-ident');
        this.error_dialog = $('#error-dialog');
        this.idle_dialog = $('#idle-dialog');
        this.deviceIndicator = $('i[data-state-indicator="device"]');
        this.wdIndicator = $('i[data-state-indicator="wd"]');
        this.mqIndicator = $('i[data-state-indicator="mq"]');
        this.wwwIndicator = $('i[data-state-indicator="www"]');
        this.state = constants.GUI_STATE_IDLE;
        this.timeout_loop_clock = 1000; // 1s
        this.timeout_loop_main =  10*60*1000; // 10 min
        this.timeout_loop_pairing = 2*60*1000; // 2 min
        this.reload_after_seconds = 60*60*3; // 3h;
        this.seconds_running = 0;
        this.gmap = new Gmap();
        this.gongAudio = null;
        this.online=true;
        this.multi_alert_fab = $('#multi-alert-fab');
        this.multi_alert_box = $('#multi-alert-box');
        this.multi_alert_holder = $('#multi-alert-holder');
        this.alarms = [];
        this.alert_feedback_holder = $('#alert-feedback-holder');
        this.alert_feedback_i = $('#alert-feedback-i');
        this.alert_feedback_d = $('#alert-feedback-d');
        this.alert_feedback_l = $('#alert-feedback-l');
        this.alert_feedback_n = $('#alert-feedback-n');
        this.stichwort = $("#stichwort");
        this.alert_triggered = $("#alert-triggered");
        this.information = $("#information");
        this.alert_stichwort = $("#alert-stichwort");
        this.einsatzort = $("#einsatzort");
        this.mak = $("#mak");
        this.feedback = $("#feedback");
        this.bma_plan_holder = $("#bma_plan_holder");
        this.route_distance_info = $('#route-distance-info');
        this.route_duration_info = $('#route-duration-info');
        this.map_weather_info = $('#map-weather-info');
        this.map_wind_info = $('#map-wind-info');
        this.objekt_doku = $('#objekt-doku');

        setInterval(function() {
            jQuery(document).trigger(constants.EVT_LOOP_CLOCK);
        }, this.timeout_loop_clock);

        jQuery(document).trigger(constants.EVT_LOOP_MAIN, true); // this is first run

        this.main_loop_interval_id = setInterval(function() {
            jQuery(document).trigger(constants.EVT_LOOP_MAIN, false); // this is all following runs
        }, this.timeout_loop_pairing);

        $('#toggle-login').click(function(){
            let oLoginForm = $('#error-dialog .login-form');
            $('#error-dialog .error-label').html('');
            if (oLoginForm.is(':visible')) {
                let username = $('#user_login').val();
                let password = $('#user_password').val();
                let token = localStorage.getItem('uuid');
                if (username.length <= 0 || password.length <= 0 || token.length <= 0) {
                    $('#error-dialog .error-label').append('<p class="text-danger">E-Mail, Passwort oder Token leer.</p>');
                } else {
                    req.loginUserToken(username, password, token).then(
                        function (response) {
                            console.log(response);
                            if (response.success) {
                                jQuery(document).trigger(constants.EVT_GUI_RESTART);
                            } else {
                                $('#error-dialog .error-label').append('<p class="text-danger">' + response.message + '</p>');
                            }
                        },
                        function (err) {
                            console.log(err);
                        }
                    );
                }
            } else {
                $('#error-dialog .error-icon').hide();
                $('#error-dialog .login-form').show();
            }
        });

    }

    hideMultiAlertHolder() {
        this.multi_alert_holder.hide()
    }

    showMultiAlertHolder() {
        this.multi_alert_holder.show()
    }

    getAlarm(einsatznummer) {
        for (let i = 0; i < this.alarms.length; i++) {
            if (this.alarms[i].einsatznummer == einsatznummer) {
                return;
            }
        }
        return null;
    }

    enableWachendisplayLayout() {
        console.log("enable Wachendisplay Layout");
        this.idle_dialog.addClass("wachendisplay");
        if ($("div.idle-container").length <= 0) {
            // not initialized - do it once
            this.idle_dialog.find('div.idle-box').prepend('<div class="idle-container"></div>');
            setTimeout(function () {
                jQuery(document).trigger(constants.EVT_CALL_WACHENDISPLAY_INIT);
            }, 5000);
        }
    }

    disableWachendisplayLayout() {
        console.log("disable Wachendisplay Layout");
        this.idle_dialog.removeClass("wachendisplay");
        $("div.idle-container").remove();
    }

    handleMultiAlert(alarm) {
        let count = 0;
        if (this.alarms != null) {
            count = this.alarms.length;
        }
        if (alarm == null || count <= 1) {
            this.multi_alert_box.hide();
            this.hideMultiAlertHolder();
            return; // Do nothing
        }
        this.multi_alert_holder.find('.multi-alert-pill').html(count);
        this.multi_alert_box.empty();
        for (let i = 0; i < count; i++ ) {
            let active = this.alarms[i].id == alarm.id;
            this.multi_alert_box.append(
                '<div id="change-alert-'+this.alarms[i].id+'" class="item'+(active ? ' active' : '')+'">' +
                '<b>' + this.alarms[i].depesche.Einsatzstichwort+'</b>'+(active ? ' (AKTIVER EINSATZ)' : '') +
                '<br/>' + this.alarms[i].depesche.Einsatzbeginn + (this.alarms[i].einsatzupdate ? ' <b/>(UPDATE!)</b>' : '') +
                '</div>'
            );
            if (!active) {
                $('#change-alert-'+this.alarms[i].id).click({ id: this.alarms[i].id, einsatzupdate: this.alarms[i].einsatzupdate }, function(evt){
                    jQuery(document).trigger(constants.EVT_ENABLE_ALERT, { id: evt.data.id, einsatzupdate: evt.data.einsatzupdate, force: true });
                });
            }
        }
        this.multi_alert_box.hide();
        this.showMultiAlertHolder();
    }

    setOnline(flag) {
        this.online = flag;
    }

    mainLoopTimingNormal() {
        clearTimeout(this.main_loop_interval_id);
        this.main_loop_interval_id = setInterval(function() {
            jQuery(document).trigger(constants.EVT_LOOP_MAIN, false);
        }, this.timeout_loop_main);
        console.log('main loop timing set to ' + this.timeout_loop_main);
    }

    hideIdle() {
        this.idle_dialog.hide();
    }

    showError(message, iconclass) {
        this.error_dialog.find("div.error-label").html('<h1>'+message+'</h1>');
        this.error_dialog.find("div.error-icon > i").removeClass();
        this.error_dialog.find("div.error-icon > i").addClass(iconclass);
        this.error_dialog.show();
    }

    showIdle() {
        this.idle_dialog.show();
    }

    hideError() {
        this.error_dialog.hide();
    }

    reload() {
        window.location.reload(true);
    }



    monitorDisplay(device) {
        if (this.state == constants.GUI_STATE_IDLE) {
            device.toggleDisplay(device.enableForWachendisplay()); // double check that screen is off when idle or enable for wachendisplay
        }
        if (this.state == constants.GUI_STATE_ALERT) {
            device.toggleDisplay(true); // double check that screen is on when alert
        }
    }

    displayDepesche(alarm, station, device) {
        if (alarm != null && alarm.depesche !== null) {
            let depesche = alarm.depesche;
            this.stichwort.empty().append(
                createH1({
                    text: depesche.Klartext
                })
            );
            this.alert_triggered.empty().append(
                createH4({
                    text: depesche.Einsatzbeginn,
                    prepend: createI({ class: "f112-icon f112-bell"})
                })
            );
            this.information.empty().append(
                createH4({
                    text: depesche.Information
                })
            );
            this.alert_stichwort.empty().append(
                createH4({
                    text: depesche.Einsatzstichwort
                })
            );

            this.einsatzort.empty().append([
                createH3({
                    text: depesche.Einsatzobjekt
                }),
                createH4({
                    append: [
                        [
                            depesche.Strasse,
                            depesche.Ort,
                            depesche.Ortsteil,
                            depesche.Patient.length > 0 ? "<b>Patient:</b> " + depesche.Patient : ""
                        ].filter(item => item.length > 0).join('<br />'),
                        createSpan({
                            id: 'einsatznummer',
                            text: 'Einsatz: ' + depesche.Einsatznummer
                        })
                    ]
                })
            ]);
            if (device.mobileDevice()) {
                this.einsatzort.append(
                    createLink({
                        href: alarm.navigationUrl(device),
                        target: '_blank',
                        class: 'btn btn-firemon',
                        text: 'Navigation starten'
                    })
                );
            }

            this.mak.empty();
            if (depesche.MitalarmierteKraefte != null && depesche.MitalarmierteKraefte.length > 0) {
                let ul = createUl();
                $.each(depesche.MitalarmierteKraefte, function(index,item) {
                    if (item.length > 0) {
                        ul.append(
                            createLi({text: item})
                        );
                    }
                });
                if (ul.children().length > 0) {
                    this.mak.append(
                        createBold({text: 'Mitalarmierte Kräfte'}),
                        ul
                    );
                }
            }

            this.bma_plan_holder.empty();
            if (depesche.Plan != null && depesche.Plan.length > 0) {
                this.bma_plan_holder.append(
                    createDiv({ class: "bma-plan", text: "Plan ", append: createBold({ text: depesche.Plan }) })
                );
                this.bma_plan_holder.show();
            } else {
                this.bma_plan_holder.hide();
            }

            if (alarm.station !== null) {
                this.initAlertFeedback(alarm.station.alert_feedback_type);
                this.feedback.show();
            } else {
                this.initAlertFeedback(0);
                this.feedback.hide();
            }
        } else {
            this.stichwort.empty();
            this.alert_triggered.empty();
            this.einsatzort.empty();
            this.information.empty();
            this.mak.empty();
            this.feedback.empty();
            this.alert_stichwort.empty();
            this.route_distance_info.empty();
            this.route_duration_info.empty();
        }
    }

    displayEinsatzobjektDetails(alarm, device) {
        this.objekt_doku.empty();

        let panelGroup = createDiv({
            class: 'panel-group col-xs-12 m-t-sm'
        });

        if (alarm.einsatzobjekt != null) {
            if (alarm.einsatzobjekt.map_no.length > 0) {
                this.bma_plan_holder.append(
                    createDiv({ class: "bma-plan", text: "Plan ", append: createBold({ text: alarm.einsatzobjekt.map_no }) })
                );
                this.bma_plan_holder.show();
            }
            if (alarm.einsatzobjekt.key_no.length > 0) {
                this.bma_plan_holder.append(
                    createDiv({ class: "bma-plan", text: "Schlüssel ", append: createBold({ text: alarm.einsatzobjekt.key_no }) })
                );
                this.bma_plan_holder.show();
            }

            if (device.mobileDevice()) {
                panelGroup.append(this.createEinsatzobjektPanel(alarm.einsatzobjekt, true));
            }
        }

        if (device.mobileDevice() && alarm.object_docu_nearby.length > 0) {
            panelGroup.append(
                alarm.object_docu_nearby.sort((a,b) => a.distance - b.distance).map(objekt => this.createEinsatzobjektPanel(objekt, false))
            );
        }

        if (device.mobileDevice()) {
            this.objekt_doku.append(panelGroup);
        }
    }

    createEinsatzobjektPanel(objekt, opened) {
        return createDiv({
            class: 'panel ' + (opened ? 'panel-danger' : 'panel-default'),
            append: [
                this.createEinsatzobjektPanelHeader(objekt, opened),
                this.createEinsatzobjektPanelBody(objekt, opened)
            ]
        });
    }
    createEinsatzobjektPanelBody(objekt, opened) {
        if (objekt === null) {
            return createDiv({
                text: 'Objekt was null in PanelBody'
            });
        }
        return createDiv({
            id: 'collapse-' + objekt.id,
            class: 'panel-collapse collapse' + (opened ? ' in' : ''),
            append: createDiv({
                class: 'panel-body',
                append: this.createEinsatzobjektHtml(objekt)
            })
        });
    }

    createEinsatzobjektPanelHeader(objekt, opened) {
        if (objekt === null) {
            return createDiv({
                text: 'Objekt was null in PanelHeader'
            });
        }
        return createDiv({
            class: 'panel-heading',
            append: createH4({
                class: 'panel-title',
                append: [
                    createLink({
                        data: {'data-toggle': 'collapse'},
                        href: '#collapse-' + objekt.id,
                        text: objekt.label.length > 0 ? objekt.label : objekt.strasse + ' ' + objekt.ort,
                        prepend: createI({
                            class: 'f112-icon f112-marker-circle',
                            styles: { 'color': 'red' }
                        })
                    }),
                    createSpan({
                        class: 'pull-right',
                        html: !opened && objekt.distance ? objekt.distance.toFixed(0) + 'm' : '<i class="f112-icon f112-firesite" style="color: red;"></i>'
                    })
                ]
            })
        })
    }

    createEinsatzobjektHtml(objekt) {
        let domCollection = [];
        if (objekt !== null) {
            domCollection.push(
                createDiv({
                    class: 'col-xs-12 col-sm-6 object',
                    append: createH3( {
                        html: [
                            objekt.label,
                            objekt.strasse,
                            objekt.ort + " " + objekt.ortsteil
                        ].join('<br />')
                    })
                })
            );

            if (objekt.documentation.images.length > 0) {
                domCollection.push(
                    createDiv({
                        class: 'col-xs-12 col-sm-6 images',
                        append: [
                            createParagraph({
                                append: createBold({ text: "Bilder" })
                            }),
                            ...objekt.documentation.images.map(image => createLink({
                                href: image.uri,
                                target: '_blank',
                                append: createImage({
                                    src: image.thumb,
                                    alt: image.label
                                })
                            }))
                        ]
                    })
                );
            }

            if (objekt.documentation.files.length > 0) {
                domCollection.push(
                    createDiv({
                        class: 'col-xs-12 col-sm-6 files',
                        append: [
                            createParagraph({
                                append: createBold({ text: "Dokumente" })
                            }),
                            createUl({
                                append: objekt.documentation.files.map(file => createLi({
                                    append: createLink({
                                        href: file.uri,
                                        target: "_blank",
                                        text: file.label
                                    })
                                }))
                            })
                        ]
                    })
                );
            }

            if (objekt.documentation.contacts.length > 0) {
                domCollection.push(
                    createDiv({
                        class: 'col-xs-12 col-sm-6 contacts',
                        append: [
                            createParagraph({
                                append: createBold({text: "Kontakte"})
                            }),
                            createUl({
                                append: objekt.documentation.contacts.map(contact => createLi({
                                    append: createBold({
                                        text: contact.label
                                    })
                                }).append([
                                    ' (' + contact.type + ')',
                                    contact.street_no,
                                    contact.zip_city,
                                    contact.mobile.length > 0 ? 'Mobil: <a href="tel:' + contact.mobile + '">' + contact.mobile + '</a>' : '',
                                    contact.phone.length > 0 ? 'Telefon: <a href="tel:' + contact.phone + '">' + contact.phone + '</a>' : '',
                                    contact.email.length > 0 ? 'E-Mail: <a href="mailto:' + contact.email + '">' + contact.email + '</a>' : ''
                                ].filter(item => item.length > 0).join('<br />')))
                            })
                        ]
                    })
                );
            }
        }
        return domCollection;
    }

    displayWeather(alarm) {
        //console.log(alarm.weather);
        this.map_weather_info.empty();
        this.map_wind_info.empty();

        if (alarm.weather !== null) {
            this.map_weather_info.append([
                createImage({
                    src: this.assets_dir + '/weather/' + this.pad(alarm.weather.WeatherIcon,2) + '-s.png'
                }),
                createParagraph({
                    text: alarm.weather.TemperatureValue+" °"+alarm.weather.TemperatureUnit
                })
            ]);
            this.map_wind_info.append(
                createParagraph({
                    html: '<i class="f112-icon f112-wind-gust"></i>' + alarm.weather.WindGustSpeedValue+" "+alarm.weather.WindGustSpeedUnit
                })
            );
        }
    }

    needsReload() {
        return this.seconds_running > this.reload_after_seconds;
    }

    pad(num, size) {
        let s = "000000000" + num;
        return s.substr(s.length-size);
    }

    depescheGongSrc(device) {
        let src = "";
        if (device.gong_type == 0) {
            // Kurz
            src = this.assets_dir + '/sound/short.mp3';
        }
        if (device.gong_type == 1) {
            // Lang
            src = this.assets_dir + '/sound/long.mp3';
        }
        return src;
    }

    updateMissionDistanceInformation(alarm) {
        this.route_distance_info.empty();
        this.route_duration_info.empty();

        if (alarm !== null && alarm.duration > 0.0) {
            this.route_distance_info.append(
                createParagraph({
                    html: '<i class="f112-icon f112-marker-circle"></i>' + (alarm.distance / 1000).toFixed(1) + ' km</p>'
                })
            );
            this.route_duration_info.append(
                createParagraph({
                    html: '<i class="f112-icon f112-hourglass"></i>' + (alarm.duration / 60).toFixed(0) + ' Min'
                })
            );
        }
    }

    initAlertFeedback(alert_feedback_type) {
        console.log('station feedback type: ' + alert_feedback_type);
        this.alert_feedback_i.on('click', function(evt){
            $('#alert-feedback-i-extra').toggle();
            $('#alert-feedback-d-extra').hide();
            $('#alert-feedback-l-extra').hide();
            $('#alert-feedback-n-extra').hide();
            evt.stopImmediatePropagation();
        });
        this.alert_feedback_d.on('click', function(evt){
            $('#alert-feedback-i-extra').hide();
            $('#alert-feedback-d-extra').toggle();
            $('#alert-feedback-l-extra').hide();
            $('#alert-feedback-n-extra').hide();
            evt.stopImmediatePropagation();
        });
        this.alert_feedback_l.on('click', function(evt){
            $('#alert-feedback-i-extra').hide();
            $('#alert-feedback-d-extra').hide();
            $('#alert-feedback-l-extra').toggle();
            $('#alert-feedback-n-extra').hide();
            evt.stopImmediatePropagation();
        });
        this.alert_feedback_n.on('click', function(evt){
            $('#alert-feedback-i-extra').hide();
            $('#alert-feedback-d-extra').hide();
            $('#alert-feedback-l-extra').hide();
            $('#alert-feedback-n-extra').toggle();
            evt.stopImmediatePropagation();
        });

        switch (alert_feedback_type) {
            case 1:
                this.alert_feedback_i.removeClass('col-xs-3').removeClass('col-xs-4').addClass('col-xs-6');
                this.alert_feedback_i.show();
                this.alert_feedback_d.hide();
                this.alert_feedback_l.hide();
                this.alert_feedback_n.removeClass('col-xs-3').removeClass('col-xs-4').addClass('col-xs-6');
                this.alert_feedback_n.show();
                this.alert_feedback_holder.show();
                break;
            case 2:
                this.alert_feedback_i.removeClass('col-xs-3').removeClass('col-xs-6').addClass('col-xs-4');
                this.alert_feedback_i.show();
                this.alert_feedback_d.hide();
                this.alert_feedback_l.removeClass('col-xs-3').removeClass('col-xs-6').addClass('col-xs-4');
                this.alert_feedback_l.show();
                this.alert_feedback_n.removeClass('col-xs-3').removeClass('col-xs-6').addClass('col-xs-4');
                this.alert_feedback_n.show();
                this.alert_feedback_holder.show();
                break;
            case 3:
                this.alert_feedback_i.removeClass('col-xs-3').removeClass('col-xs-6').addClass('col-xs-4');
                this.alert_feedback_i.show();
                this.alert_feedback_d.removeClass('col-xs-3').removeClass('col-xs-6').addClass('col-xs-4');
                this.alert_feedback_d.show();
                this.alert_feedback_l.hide();
                this.alert_feedback_n.removeClass('col-xs-3').removeClass('col-xs-6').addClass('col-xs-4');
                this.alert_feedback_n.show();
                this.alert_feedback_holder.show();
                break;
            case 4:
                this.alert_feedback_i.removeClass('col-xs-4').removeClass('col-xs-6').addClass('col-xs-3');
                this.alert_feedback_i.show();
                this.alert_feedback_d.removeClass('col-xs-4').removeClass('col-xs-6').addClass('col-xs-3');
                this.alert_feedback_d.show();
                this.alert_feedback_l.removeClass('col-xs-4').removeClass('col-xs-6').addClass('col-xs-3');
                this.alert_feedback_l.show();
                this.alert_feedback_n.removeClass('col-xs-4').removeClass('col-xs-6').addClass('col-xs-3');
                this.alert_feedback_n.show();
                this.alert_feedback_holder.show();
                break;
            default:
                console.log('no feedback mode enabled');
                this.alert_feedback_holder.hide();
                break;
        }
    }

}

