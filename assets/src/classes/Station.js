import { reportError } from '../util/requests';

export default class Station {
    constructor() {
        this.id = 0;
        this.name = "not assigned";
        this.long = 0.0;
        this.lat = 0.0;
        this.alert_feedback_type = 0;
        this.mak_replacements = {
            "10-83-21": "Johanniter RTW 10-83-21",
            "10-83-22": "Johanniter RTW 10-83-22",
            "10-83-02": "RTW Schleswig 10-83-02",
            "10-82-01": "NEF DRK 10-82-01",
            "10-82-02": "NEF Reserve DRK 10-82-02",
            "10-83-01": "RTW DRK 10-83-01",
            "10-83-03": "RTW DRK 10-83-03",
            "10-83-04": "RTW Reserve DRK 10-83-04",
            "10-83-05": "RTW Reserve DRK 10-83-05",
            "10-83-06": "RTW Reserve DRK 10-83-06",
            "10-83-07": "RTW DRK 10-83-07",
            "10-83-11": "RTW DRK 10-83-11",
            "11-83-01": "RTW DRK 11-83-01",
            "44-83-01": "RTW Eggebek 44-83-01",
            "44-83-02": "RTW Eggebek 44-83-02",
            "20-82-01": "NEF Kappeln 20-82-01",
            "20-83-01": "RTW Kappeln 20-83-01",
            "20-83-02": "RTW Kappeln 20-83-02",
            "12-83-01": "RTW Kropp 12-83-01",
            "40-83-01": "RTW Medelby 40-83-01",
            "12-83-05": "RTW Norderstapel 12-83-05",
            "30-83-01": "RTW Sörup 30-83-01",
            "23-83-01": "RTW Steinbergkirche 23-83-01",
            "23-83-02": "RTW 23-83-02",
            "20-83-11": "RTW Kappeln 20-83-11",
            "Flo FL 50-83-01": "BF RTW 1",
            "Flo FL 50-83-02": "BF RTW 2",
            "Flo FL 50-83-03": "BF RTW 3",
            "Flo FL 50-83-04": "BF RTW 4",
            "Flo FL 50-83-05": "BF RTW 5",
            "Flo FL 50-83-06": "BF RTW 6",
            "Flo FL 50-83-07": "BF RTW 7",
            "Flo FL 50-83-08": "BF RTW 8",
            "Flo FL 50-83-09": "BF RTW 9",
            "Flo FL 50-83-10": "BF RTW 10",
            "Flo FL 50-83-20": "BF Reservewagen",
            "Flo FL 52-82-01": "BF NEF",
            "Flo FL 52-82-02": "Reserve NEF",
            "RetFL 58-82-01": "Bundeswehr NEF",
            "RetSH 71-84-01": "VEF",
            "SEG Höhenrettung": "BF Höhenretter",
            "SEG Tauchen": "BF Taucher",
            "SEG Schiffsbrandbekämpfung": "BF Schiffbrandbekämpfung",
            "SEG ABC Erkundung": "BF ABC Erkundung",
            "RetFL 51-83-01": "Falck RTW 1",
            "RetFL 51-83-02": "Falck RTW 2",
            "Flo FL 50-11-01": "BF ELW",
            "Flo FL 50-46-01": "BF", // depesche: Flo FL 50-46-01 (HLF 1)
            "Flo FL 50-48-01": "BF", // depesche: Flo FL 50-48-01 (HLF 2/TH)
            "Flo FL 50-32-01": "BF DLK",
            "Flo FL 50-59-01": "BF GWH",
            "\\d{5,}": "",
            "\\d{2}\\.\\d{2}\\.\\d{4}\\s\\d{2}\\:\\d{2}\\:\\d{2}":""
        };
        this.speech_replacements = {
            "FEU": "Feuer",
            "FF": "Feuerwehr",
            "AWF": "Amtswehrführer",
            "BF": "B F ",
            "RTW": "R T W ",
            "NEF": "N E F ",
            "RetSL": "Rettung",
            "TH": "Technische Hilfe",
            "AED": "A E D ",
            "Vollalarm": "",
            "GWF": "Gemeindewehrführer",
            "ORGL": "Orgleiter",
            "LNA": "Leitender Notarzt",
            "Flo FL":"Florian Flensburg",
            "NA":"Notarzt",
            "BAB":"B A B",
        };
    }

    update(data) {
        try {
            this.id = data.id;
            this.name = data.name;
            this.long = data.long;
            this.lat = data.lat;
            this.alert_feedback_type = data.alert_feedback_type;
        } catch (e) {
            reportError(this, e.message, 'update in Station', 0, 0, e.name);
        }
    }

    ready() {
        return this.id > 0;
    }

    mapReplace(str, map) {
        let RE = new RegExp(Object.keys(map).join("|"), "g");
        return str.replace(RE, function(matched) {
            let repl = map[matched];
            if (repl === undefined) {
                repl = "";
            }
            return repl;
        });
    }

    speechReplace(str) {
        return this.mapReplace(str, this.speech_replacements);
    }

    makReplace(str) {
        return this.mapReplace(str, this.mak_replacements);
    }

    depescheSpeechText(alarm) {
        if (alarm.depesche == null) {
            return "";
        }
        let speechText = "";
        if (alarm.depesche.Klartext.length > 0) {
            if (alarm.depesche.Klartext.indexOf("   ")>-1) {
                let parts = alarm.depesche.Klartext.split("   ");
                parts.shift();
                speechText += this.speechReplace(parts.join(" "));
            } else {
                speechText += this.speechReplace(alarm.depesche.Klartext);
            }
        }
        if (alarm.depesche.Information.length > 0) {
            speechText += ", " + alarm.depesche.Information;
        }
        if (alarm.depesche.Einsatzobjekt.length > 0) {
            speechText += ", " + alarm.depesche.Einsatzobjekt;
        }
        if (alarm.depesche.Strasse.length > 0) {
            speechText += ", " + alarm.depesche.Strasse;
        }
        if (alarm.depesche.Ort.length > 0) {
            speechText += ", " + alarm.depesche.Ort;
        }
        if (alarm.depesche.Ortsteil.length > 0) {
            speechText += ", Ortsteil " + alarm.depesche.Ortsteil;
        }
        if (alarm.depesche.Plan.length > 0) {
            speechText += ", Plan-Nummer " + alarm.depesche.Plan.replace("-", " Strich ") + " verwenden";
        }
        if (alarm.depesche.MitalarmierteKraefte.length > 0) {
            speechText += this.speechReplace(". Mitalarmierte Kräfte: " + alarm.depesche.MitalarmierteKraefte.map(
                item => {
                    return item.replace(/([\d{1,}\.]){2,}|([\d{1,}\:]){2,}|([\d{1,}\-]){2,}/g, "").trim();
                }).join(", ")
            );
        }
        // finally remove annoying chars
        speechText = speechText.replace(/\(/g, '').replace(/\)/g, '');
        return speechText;
    }
}