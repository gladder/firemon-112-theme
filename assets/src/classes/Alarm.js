import * as constants from '../util/constants';

export default class Alarm {
    constructor() {
        this.depesche = null;
        this.weather = null;
        this.id = 0;
        this.disable_timeout_id = null;
        this.distance = 0.0;
        this.duration = 0.0;
        this.speech_done_count = 0;
        this.speechAudio = null;
        this.einsatzbeginnDatetime = null;
        this.einsatzupdate = false;
        this.einsatzobjekt = null;
        this.object_docu_nearby = [];
        this.station = null;
    }

    lat() {
        if (this.depesche != null) {
            return this.depesche.Lat;
        }
        return 0.0;
    }

    long() {
        if (this.depesche != null) {
            return this.depesche.Long;
        }
        return 0.0;
    }

    einsatznummer() {
        if (this.depesche != null && this.depesche.Einsatznummer != null && this.depesche.Einsatznummer.length > 0) {
            return this.depesche.Einsatznummer;
        }
        return "";
    }

    einsatzbeginn() {
        if (this.einsatzbeginnDatetime == null && this.depesche != null) {
            if (this.depesche.Einsatzbeginn != null && this.depesche.Einsatzbeginn.length > 0) {
                // format: 18.03.2021 16:17:41
                let parts = this.depesche.Einsatzbeginn.trim().split(' ');
                if (parts != null && parts.length == 2) {
                    let dateParts = parts[0].split('.');
                    this.einsatzbeginnDatetime = new Date(dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0] + 'T' + parts[1]);
                }
            }
        }
        return this.einsatzbeginnDatetime;
    }

    active() {
        return this.id > 0;
    }

    navigationUrl(device) {
        if (device.getClientType() == constants.CLIENT_TYPE_WEB) {
            return "https://www.google.com/maps?saddr=My+Location&daddr="+this.lat()+","+this.long();
        }
        if (device.getClientType() == constants.CLIENT_TYPE_ANDROID) {
            return "https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&destination="+this.lat()+","+this.long();
        }
        if (device.getClientType() == constants.CLIENT_TYPE_IOS) {
            return "https://maps.apple.com/?dirflg=d&daddr="+this.lat()+","+this.long();
        }
        return "";
    }

}