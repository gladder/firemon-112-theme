import * as constants from "../util/constants";

export default class Gmap {
    constructor() {
        this.directionsService = null;
        this.streetviewService = null;
        this.directionsRenderer = null;
        this.map = null;
        this.route = null;
        this.trafficLayer = null;
        this.marker = [];
        this.info = [];
        this.routeTilesLoaded = false;
        this.routeDirectionLoaded = false;
        this.routeMarkerLoaded = false;
        this.mapTilesLoaded = false;
        this.mapWindLoaded = false;
        this.mapMarkerLoaded = false;
        this.mapPanorama = null;
        this.screenshotRouteTimeoutId = null;
        this.screenshotMapTimeoutId = null;
    }

    resetRouteLoadingStates() {
        this.routeTilesLoaded = false;
        this.routeDirectionLoaded = false;
        this.routeMarkerLoaded = false;
    }

    resetMapLoadingStates() {
        this.mapTilesLoaded = false;
        this.mapWindLoaded = false;
        this.mapMarkerLoaded = false;
    }

    triggerRouteScreenshot() {
        if (this.routeTilesLoaded && this.routeMarkerLoaded && this.routeDirectionLoaded) {
            if (this.screenshotRouteTimeoutId) {
                clearTimeout(this.screenshotRouteTimeoutId);
            }
            this.screenshotRouteTimeoutId = setTimeout(function() {
                jQuery(document).trigger(constants.EVT_ROUTE_SCREENSHOT);
            }, 3500);
        } else {
            console.log("cannot make screenshot of route now");
            console.log("routeTilesLoaded", this.routeTilesLoaded);
            console.log("routeDirectionLoaded", this.routeDirectionLoaded);
            console.log("routeMarkerLoaded", this.routeMarkerLoaded);
        }
    }

    triggerMapScreenshot() {
        if (this.mapMarkerLoaded && this.mapTilesLoaded && this.mapWindLoaded) {
            if (this.screenshotMapTimeoutId) {
                clearTimeout(this.screenshotMapTimeoutId);
            }
            this.screenshotMapTimeoutId = setTimeout(function() {
                jQuery(document).trigger(constants.EVT_MAP_SCREENSHOT);
            }, 3500);
        } else {
            console.log("cannot make screenshot of map now");
            console.log("cannot make screenshot of route now");
            console.log("mapTilesLoaded", this.mapTilesLoaded);
            console.log("mapWindLoaded", this.mapWindLoaded);
            console.log("mapMarkerLoaded", this.mapMarkerLoaded);
        }
    }

    haversine_distance(mk1, mk2) {
        return this.haversine_distance_raw(mk1.position.lat(), mk1.position.lng(), mk2.position.lat(), mk2.position.lng());
    }

    haversine_distance_raw(lat1, lng1, lat2, lng2) {
        let R = 6371.0710; // Radius of the Earth in miles
        let rlat1 = lat1 * (Math.PI/180); // Convert degrees to radians
        let rlat2 = lat2 * (Math.PI/180); // Convert degrees to radians
        let difflat = rlat2-rlat1; // Radian difference (latitudes)
        let difflon = (lng2-lng1) * (Math.PI/180); // Radian difference (longitudes)

        return 2 * R * Math.asin(Math.sqrt(Math.sin(difflat/2)*Math.sin(difflat/2)+Math.cos(rlat1)*Math.cos(rlat2)*Math.sin(difflon/2)*Math.sin(difflon/2)));
    }

}