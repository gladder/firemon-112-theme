import * as constants from '../util/constants';
import { reportError } from '../util/requests';

export default class Device {

    constructor() {
        this.uuid = Device.getUuid();
        this.id = 0;
        this.print = false;
        this.print_count = 0;
        this.alarm_timeout = 0;
        this.watchdog = false;
        this.hdmi_cec = false;
        this.name = "unknown";
        this.type = constants.TYPE_MONITOR;
        this.state = constants.STATE_UNKNOWN;
        this.ips = [];
        this.speech_enabled = false;
        this.speech_count = 0;
        this.speech_interval = 0;
        this.speech_delay = 0;
        this.gong_type = 0;
        this.io_watchdog = null;
        this.station_id = 0;
        this.mq_id = 0;
        this.watchdog_version = '';
        this.resolution = null;
        this.hdmi_legacy = false;
        this.user_id = null;
        this.wachendisplay_id = null;
        this.wachendisplay_todays_slots_time_series = [];
        this.background = null;
        this.gpio_alert_triggers = [];
    }

    updateConfig(data) {
        try {
            if (this.uuid == data.token) {
                this.id = data.id;
                this.name = data.name;
                this.user_id = data.user_id;
                this.wachendisplay_id = data.wachendisplay_id;
                if (this.isInjectedAlert()) {
                    this.print = false; // injected alert devices are not allowed to print
                    this.print_count = 0;
                } else {
                    this.print = data.print_depesche;
                    this.print_count = data.print_depesche_count;
                }

                this.alarm_timeout = data.alert_timeout * 60 * 1000;
                this.hdmi_cec = data.hdmi_cec;
                this.hdmi_legacy = data.hdmi_legacy;
                if (this.isInjectedAlert()) {
                    this.speech_enabled = false; // injected alerts are not allowed to speech
                    this.speech_count = 0;
                    this.speech_interval = 0;
                    this.speech_delay = 0;
                } else {
                    this.speech_enabled = data.depesche_speech_enabled;
                    this.speech_count = data.depesche_speech_count;
                    this.speech_interval = data.depesche_speech_interval;
                    this.speech_delay = data.depesche_speech_delay;
                }
                this.gong_type = data.depesche_gong_type;
                this.type = data.type;
                this.mq_reconnect_attempts = 3; // always try to connect 3 times
                this.background = data.background;
                this.gpio_alert_triggers = data.gpio_alert_triggers;

                jQuery(document).trigger(constants.EVT_DEVICE_CONFIG_UPDATED);

                if (this.state != data.state) {
                    this.state = data.state;
                    jQuery(document).trigger(constants.EVT_STATE_CHANGED);
                }

                if (this.watchdog != data.watchdog) {
                    if (this.isInjectedToken()) {
                        this.watchdog = false; // injected token devices are not allowed to have watchdog
                    } else {
                        this.watchdog = data.watchdog;
                        jQuery(document).trigger(constants.EVT_DEVICE_WATCHDOG_UPDATED);
                    }
                }

                if (this.station_id != data.station_id) {
                    this.station_id = data.station_id;
                    jQuery(document).trigger(constants.EVT_STATION_CHANGED);
                }

                if (this.mq_id != data.mq_id) {
                    if (this.isInjectedAlert()) {
                        this.mq_id = 0; // injected alerts are not allowed to have message-queue
                    } else {
                        this.mq_id = data.mq_id;
                        jQuery(document).trigger(constants.EVT_MQ_CHANGED);
                    }
                }
            }
        } catch (e) {
            reportError(this, e.message, 'updateConfig in Device', 0, 0, e.name);
        }
    }

    /**
     * @returns {boolean}
     */
    dependsMq() {
        return this.mq_id > 0 && this.type == constants.TYPE_MONITOR;
    }

    enableForWachendisplay() {
        if (this.wachendisplay_id != null && this.wachendisplay_todays_slots_time_series.length > 0) {
            let now = new Date();
            for (let i = 0; i < this.wachendisplay_todays_slots_time_series.length; i++) {
                let from = new Date();
                let parts = this.wachendisplay_todays_slots_time_series[i].from.split(':');
                from.setHours(parts[0], parts[1], parts[2]);
                let to = new Date();
                parts = this.wachendisplay_todays_slots_time_series[i].to.split(':');
                to.setHours(parts[0], parts[1], parts[2]);
                //console.log(now, from, to);
                //console.log(now.getTime(), from.getTime(), to.getTime());
                if (now.getTime() >= from.getTime() && now.getTime() <= to.getTime()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @returns {boolean}
     */
    active() {
        return this.state == constants.STATE_ACTIVE;
    }

    pending() {
        return this.state == constants.STATE_PENDING;
    }

    disabled() {
        return this.state == constants.STATE_DISABLED;
    }

    ready() {
        return this.id > 0 && this.active() && ( !this.dependsWatchdog() || (this.dependsWatchdog() && this.watchdocConnected() ) );
    }

    mobileDevice() {
        return this.type == constants.TYPE_MOBILE;
    }

    printerCapableDevice() {
        return this.type == constants.TYPE_MONITOR;
    }

    watchdogMonitorDevice() {
        return this.type == constants.TYPE_MONITOR && this.watchdocConnected();
    }

    dependsWatchdog() {
        return this.watchdog && this.type == constants.TYPE_MONITOR;
    }

    watchdocConnected() {
        return this.io_watchdog != null && this.io_watchdog.connected;
    }

    wachendisplayEnabled() {
        return this.wachendisplay_id != null && this.wachendisplay_id > 0;
    }

    toggleDisplay(flag) {
        console.log("Toggle monitor " + (flag ? 'on' : 'off') + " (if watchdog is active...)");
        if (this.watchdog && (this.hdmi_cec || this.hdmi_legacy)) {
            jQuery(document).trigger(constants.EVT_TOGGLE_DISPLAY, flag);
        }
    }

    isInjectedToken() {
        if (injectToken == null || injectToken.length <= 0) {
            return false;
        }
        return injectToken == this.uuid;
    }

    isInjectedAlert() {
        return injectAlert != null && injectAlert > 0;
    }

    getClientType() {
        if (injectClientType != null && injectClientType.length > 0) {
            return injectClientType; // can be android and ios - see constants
        }
        return constants.CLIENT_TYPE_WEB;
    }

    static getUuid() {
        if (injectToken != null && injectToken.length > 0) {
            return injectToken; // injectToken will always win but never be stored or even delete existing token.
        }
        if (localStorage.getItem('reset_uuid') == 'true') {
            localStorage.removeItem('reset_uuid');
            localStorage.removeItem('uuid');
            console.log("Scheduled UUID reset performed...");
        }

        if (setupExternalToken != null && setupExternalToken.length == 32) {
            localStorage.setItem('uuid', setupExternalToken); // if good enough external token existing store and use that instead of creating own uuid
            console.log("setup external token " + setupExternalToken);
            return setupExternalToken;
        }

        let uuid = localStorage.getItem('uuid');
        if (uuid == null || uuid.length <= 0) {
            uuid = Device.makeid(32);
            localStorage.setItem('uuid', uuid);
        }
        return uuid;
    }

    static makeid(length) {
        let result           = '';
        let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
}