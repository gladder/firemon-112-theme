const dropArea = $('div.drag-area'),
    dragText = $('div.drag-area > header'),
    uploadButton = $('div.drag-area > button'),
    uploadInput = $('div.drag-area > input'),
    uploadFileForm = $('form.uploadFileForm');
let files;

uploadButton.on('click',(evt) => {
    evt.preventDefault();
    uploadInput.click();
});

dropArea.on('dragover',(evt) => {
    evt.preventDefault();
    dropArea.addClass("active");
    dragText.text("Droppen für Upload");
});

dropArea.on('dragleave',(evt) => {
    evt.preventDefault();
    dropArea.removeClass("active");
    dragText.text("Upload per Drag & Drop");
});

dropArea.on('drop',(evt) => {
    evt.preventDefault();
    uploadInput.prop('files', evt.originalEvent.dataTransfer.files);
    files = evt.originalEvent.dataTransfer.files;
    uploadInput.prop('files', files);
    showFile();
});

uploadInput.on('change',(evt) => {
    files = evt.target.files;
    dropArea.addClass("active");
    showFile();
});

function showFile() {
    if (files == undefined || files.length <= 0) {
        // no file
        dragText.text("keine Datei für upload");
        dropArea.removeClass("active");
    } else if (files.length == 1) {
        // single file
        dragText.text(files[0].name);
        uploadFileForm.submit();
    } else {
        // multiple files
        dragText.text(files.length + " Dateien");
        uploadFileForm.submit();
    }
}

function resetUpload() {
    files = [];
    showFile();
    initSortableTableRow();
}