function persistPolygon() {
    let polyJson = polygon2Json(polygon);
    let polyCenter = polygonCenter(polygon);

    $('input[name="long"]').val(polyCenter.lng);
    $('input[name="lat"]').val(polyCenter.lat);
    $('input[name="coords"]').val(polyJson);
    $('#lb_long').text(polyCenter.lng.toFixed(6));
    $('#lb_lat').text(polyCenter.lat.toFixed(6));
    $('#lb_coords').text(polyJson);

    setBrMarker(polygonCenter(polygon));
}

function polygon2Json(polygon) {
    let coords = [];
    try {
        polygon.getPath().getArray().forEach(item => {
            coords.push({lng: item.lng(), lat: item.lat()});
        });
    } catch (e) { console.error(e); }
    return JSON.stringify(coords);
}

function polygonCenter(polygon) {
    let coord_min = { lng: 180.0, lat: 180.0 };
    let coord_max = { lng: -180.0, lat: -180.0 };
    try {
        polygon.getPath().getArray().forEach(item => {
            if (item.lng() < coord_min.lng) {
                coord_min.lng = item.lng();
            }
            if (item.lng() > coord_max.lng) {
                coord_max.lng = item.lng();
            }

            if (item.lat() < coord_min.lat) {
                coord_min.lat = item.lat();
            }
            if (item.lat() > coord_max.lat) {
                coord_max.lat = item.lat();
            }
        });
    } catch (e) { console.error(e); }
    return {
        lng: coord_min.lng + ( (coord_max.lng - coord_min.lng) / 2.0 ),
        lat: coord_min.lat + ( (coord_max.lat - coord_min.lat) / 2.0 )
    };
}

function json2polygon(json) {
    let coords = JSON.parse(json);
    let coordArray = [];
    coords.forEach( item => {
        coordArray.push(
            new google.maps.LatLng(item.lat,item.lng)
        );
    });
    console.log(coordArray);
    let poly = new google.maps.Polygon(polyOpts);
    poly.setPath(coordArray);
    polygonAddEvents(poly);
    return poly;
}

function extractGeocodeAddress(geocodingResponse) {
    try {
        // If the response is null, not an array, or empty, return sensible fallbacks.
        if (!geocodingResponse || !Array.isArray(geocodingResponse) || geocodingResponse.length === 0) {
            return { street: "No address available", city: "No address available" };
        }

        // Use the first result as the primary candidate.
        const result = geocodingResponse[0];

        // Ensure address_components exists.
        const components = result.address_components;
        if (!components || !Array.isArray(components) || components.length === 0) {
            if (result.formatted_address) {
                const parts = result.formatted_address.split(",");
                return {
                    street: parts[0] ? parts[0].trim() : "No street info",
                    city: parts[1] ? parts[1].trim() : result.formatted_address
                };
            }
            return { street: "No street info", city: "No city info" };
        }

        // Initialize variables for desired address parts.
        let route = null,
            streetNumber = null,
            postalCode = null,
            locality = null;

        // Loop over each component and extract values based on their types.
        components.forEach(component => {
            if (component && Array.isArray(component.types)) {
                if (component.types.includes("route")) {
                    route = component.long_name;
                }
                if (component.types.includes("street_number")) {
                    streetNumber = component.long_name;
                }
                if (component.types.includes("postal_code")) {
                    postalCode = component.long_name;
                }
                if (component.types.includes("locality")) {
                    locality = component.long_name;
                }
            }
        });

        // Build the street string.
        let street = "";
        if (route || streetNumber) {
            if (route && streetNumber) {
                // In many locales the street name comes first followed by the house number.
                street = `${route} ${streetNumber}`;
            } else {
                street = route || streetNumber;
            }
        } else if (result.formatted_address) {
            const parts = result.formatted_address.split(",");
            street = parts[0] ? parts[0].trim() : "No street info";
        } else {
            street = "No street info";
        }

        // Build the city string (including ZIP code if available).
        let city = "";
        if (locality || postalCode) {
            if (postalCode && locality) {
                city = `${postalCode} ${locality}`;
            } else {
                city = locality || postalCode;
            }
        } else if (result.formatted_address) {
            const parts = result.formatted_address.split(",");
            city = parts.length > 1 ? parts[1].trim() : result.formatted_address;
        } else {
            city = "No city info";
        }

        return { street, city };

    } catch (error) {
        // Log the error if needed and return default fallback values.
        console.error("Error in extractAddress:", error);
        return { street: "No street info", city: "No city info" };
    }
}