
$('.module-config').on('click', function(){
    let configDiv = $(this).next('.cfg-holder');
    configDiv.toggle();
    console.log(configDiv);
});
$('.cfg-holder').on('input', 'input, select', function() {
    let module = $(this).closest('.function-holder');
    let value = "";
    if ($(this).is(':checkbox')) {
        value = $(this).is(':checked') ? 'true' : 'false';
    } else {
        value = $(this).val();
    }
    module.data('cfg_' + $(this).attr('id'), value);
    persistSettings();
});


$('*[data-id]').each(function(){
    markIntervalsCell($(this));
    $(this).on('click',function(){
        $(this).data('selected', !$(this).data('selected'));
        markIntervalsCell($(this));
        collectIntervalData();
    });
    $(this).on('mouseover',function(){
        if ($(this).parent().data('drag') == true) {
            $(this).data('selected', $(this).parent().data('dragmode'));
            markIntervalsCell($(this));
            collectIntervalData();
        }
    });
    $(this).on('mousedown',function(){
        $(this).parent().data('drag', true);
        $(this).parent().data('dragmode', $(this).data('selected'));
    });
    $(this).on('mouseup',function(){
        $(this).parent().data('drag', false);
    });
});
collectIntervalData();

$('*[data-preview-variant]').each(function(){
    $(this).on('click',function(){
        changeLayoutMode($(this).data('preview-variant'),$(this).data('cols'));
    });
});

changeLayoutMode();
enableDropForHolder();

function placeModules(modules) {
    modules = modules.sort((a, b) => {
        if (a.sort < b.sort) {
            return -1;
        }
    });

    for (var i = 0; i < modules.length; i++) {
        var module = modules[i];
        let id = module.id;
        let target = $('[data-cell="'+module.position+'"]');
        if (!target.length) {
            target = $('[data-cell="1"]');
        }
        $('#' + id).appendTo(target);
    }
}

function markIntervalsCell(jqObj) {
    if (jqObj.data('selected') == true) {
        jqObj.css('background-color', 'lime');
    } else {
        jqObj.css('background-color', '#efefef');
    }
}

function collectIntervalData() {
    let res = [];
    $('*[data-dow]').each(function(){
        let item = {}
        item.dow = $(this).data('dow');
        item.on = [];
        $(this).children().each(function(){
            if ($(this).data('selected') == true) {
                item.on.push($(this).data('id'));
            }
        });
        res.push(item);
    });
    $('input[name="enable_schedule_json"]').val(JSON.stringify(res));
}

function changeLayoutMode(variant = -1, cols_string = "") {
    $('*[data-preview-variant]').each(function(){
        $(this).css('background-color', 'transparent');
    });

    let initialModuleLayoutStr = $('input[name="module_layout_json"]').val();
    if (initialModuleLayoutStr.length <= 0) {
        initialModuleLayoutStr = '{ "variant": 1, "modules": [] }';
    }
    //console.log(initialModuleLayoutStr);
    let initialModuleLayout = JSON.parse(initialModuleLayoutStr);
    //console.log(initialModuleLayout);
    if (!initialModuleLayout.hasOwnProperty('modules')) {
        initialModuleLayout.modules = [];
    }
    if (!initialModuleLayout.hasOwnProperty('variant')) {
        initialModuleLayout.variant = 1;
    }

    if (variant <= -1) {
        variant = initialModuleLayout.variant;
    }
    //console.log("initial variant", variant);

    if (cols_string.length <= 0) {
        cols_string = $('.dashboard[data-preview-variant="'+variant+'"]').data('cols');
    }

    //console.log("cols_string", cols_string);

    $('*[data-preview-variant="'+variant+'"]').css('background-color','lime');

    // move places modules back to holder

    for (var i = 0; i < initialModuleLayout.modules.length > 0; i++) {
        $('#' + initialModuleLayout.modules[i].id).appendTo($('.module-holder'));
    }

    let cols = [];
    if (cols_string.toString().indexOf("|") !== -1) {
        cols = cols_string.split("|");
    } else {
        cols.push(cols_string);
    }
    let div = $('<div></div>');
    div.addClass('dashboard');
    div.attr('data-variant',variant);

    var topRow = $('<div></div>');
    var bottomRow = $('<div></div>');
    var cell_counter = 1;
    for (var i = 0; i < cols.length; i++) {
        var topDiv = $('<div></div>');
        var bottomDiv = $('<div></div>');

        topDiv.attr('data-col', cols[i]);
        topDiv.addClass('droppable');
        topDiv.attr('data-cell', cell_counter);
        topRow.append(topDiv);
        cell_counter++;

        bottomDiv.attr('data-col', cols[i]);
        bottomDiv.addClass('droppable align-bottom');
        bottomDiv.attr('data-cell', cell_counter);
        bottomRow.append(bottomDiv);
        cell_counter++;
    }
    div.append(topRow);
    div.append(bottomRow);

    $('#layout_canvas').html(div);
    placeModules(initialModuleLayout.modules);
    loadModuleConfigs(initialModuleLayout.modules);
    enableDragDropForLayout();
    //console.log('changed layout mode',collectLayoutData());
    $('input[name="module_layout_json"]').val(JSON.stringify(collectLayoutData()));
}

function loadModuleConfigs(modules) {
    for (var i = 0; i < modules.length; i++) {
        var module = modules[i];
        if (module.hasOwnProperty('cfg')) {
            let keys = Object.keys(module.cfg);
            if (keys.length > 0) {
                console.log("module has config: ", module);
                let moduleHolder = $('#' + module.id);
                if (moduleHolder.length) {
                    for (let key of keys) {
                        if (module.cfg[key] == "true") {
                            $('#' + key).prop('checked', true);
                        } else if (module.cfg[key] == "false") {
                            $('#' + key).prop('checked', false);
                        } else {
                            $('#' + key).val(module.cfg[key]);
                        }
                        moduleHolder.data('cfg_' + key, module.cfg[key]);
                    }
                }
            }
        }
    }
}

function enableDropForHolder() {
    $('.module-holder').on('dragenter', function () {
        $(this).css('background-color', 'lime');
    });
    $('.module-holder').on('dragover', function (event) {
        //event.dataTransfer.setData("Text",event.target.id);
        let evt = event.originalEvent;
        evt.preventDefault();
        document.body.style.cursor = 'move';
        return false;
    });

    $('.module-holder').on('dragleave', function () {
        //event.dataTransfer.setData("Text",event.target.id);
        $(this).css('background-color', '#efefef');
    });

    $('.module-holder').on('drop', function (event) {
        let evt = event.originalEvent;
        //event.dataTransfer.setData("Text",event.target.id);
        evt.preventDefault();
        //retrieve text "id" of dragged element using the DataTransfer Object*/
        var data = evt.dataTransfer.getData("Text");
        $('#' + data).appendTo(evt.target);
        let layoutData = collectLayoutData();
        $('input[name="module_layout_json"]').val(JSON.stringify(layoutData));
        $(this).css('background-color', '#efefef');
        return false;
    });


    $('.module-holder').on('dragend', function () {
        document.body.style.cursor = 'default';
        $('.droppable').each(function () {
            $(this).css('background-color', '#efefef');
        });
    });
}
function enableDragDropForLayout() {
    $('.draggable').each(function () {
        $(this).attr('draggable', true);
    });
    $('.draggable').on('dragstart', function (event) {
        let evt = event.originalEvent;
        evt.dataTransfer.dropEffect = 'move';
        evt.dataTransfer.setData("Text", $(this).attr('id'));
        document.body.style.cursor = 'move';
    });

    $('.droppable').on('dragenter', function () {
        $(this).css('background-color', 'lime');
    });

    $('.droppable').on('dragover', function (event) {
        //event.dataTransfer.setData("Text",event.target.id);
        let evt = event.originalEvent;
        evt.preventDefault();
        document.body.style.cursor = 'move';
        return false;
    });

    $('.droppable').on('dragleave', function () {
        //event.dataTransfer.setData("Text",event.target.id);
        $(this).css('background-color', 'transparent');
    });

    $('.droppable').on('drop', function (event) {
        let evt = event.originalEvent;
        //event.dataTransfer.setData("Text",event.target.id);
        evt.preventDefault();
        //retrieve text "id" of dragged element using the DataTransfer Object*/
        var data = evt.dataTransfer.getData("Text");
        $('#' + data).appendTo(evt.target);
        persistSettings();
        $(this).css('background-color', 'transparent');
        return false;
    });


    $('.draggable').on('dragend', function () {
        document.body.style.cursor = 'default';
        $('.droppable').each(function () {
            $(this).css('background-color', 'transparent');
        });
    });
}

function persistSettings() {
    let layoutData = collectLayoutData();

    console.log(layoutData);
    $('input[name="module_layout_json"]').val(JSON.stringify(layoutData));
}

function collectLayoutData() {
    let res = {
        variant: parseInt($('.dashboard[data-variant]').data('variant')),
        modules: []
    };

    // set data-sort
    $('.dashboard .droppable').each(function(){
        let sort_cnt = 0;
        $(this).children().each(function(){
            $(this).data('sort', sort_cnt);
            sort_cnt++;
        });

    });

    $('.dashboard .draggable').each(function(){
        let item = {}
        item.id = $(this).attr('id');
        item.position = $(this).parent().data('cell');
        item.sort = $(this).data('sort');
        item.cfg = {};
        $.each($(this).data(), function(key, value){
            if (key.startsWith('cfg_')) {
                item.cfg[key.replace('cfg_','')] = value;
            }
        });
        res.modules.push(item);
    });
    //console.log(res);
    return res;
}