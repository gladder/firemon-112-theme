$(document).ready(function() {
    initSortableTableRow();
});

function initSortableTableRow() {
    $("#sortableTable tbody").sortable({
        handle: '.sortHandle',
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        stop: function (event, ui) {
            var sortOrder = $(this).sortable('toArray', {attribute: 'data-id'});
            $(this).request('onUpdateSortOrder', {
                data: { sortOrder: sortOrder }
            });
        },
    });
}