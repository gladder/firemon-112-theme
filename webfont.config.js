let path = require('path');
let webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = [
    {
        entry: './assets/src/webfont.js',
        module: {
            rules: [
                {
                    test: /\.font\.js/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {
                                publicPath: './',
                            }
                        },
                        {
                            loader: 'css-loader',
                            options: {
                                url: false,
                            }
                        },
                        {
                            loader: 'webfonts-loader',
                            options: {
                            }
                        }
                    ]
                },
            ]
        },
        resolve: {
            extensions: ['*', '.js']
        },
        mode: 'production',
        output: {
            path: path.resolve(__dirname, './assets/javascript/'),
            filename: 'f112-icons.font.js',
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: '../css/f112icons-font.css'
            })
        ]
    },

];