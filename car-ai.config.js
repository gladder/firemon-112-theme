let path = require('path');
let webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = [
    {
        entry: './assets/src/car-ai.js',
        resolve: {
            extensions: ['*', '.js']
        },
        mode: 'production',
        output: {
            path: path.resolve(__dirname, './assets/javascript/'),
            filename: 'car-ai.js',
        },
        plugins: [
            new webpack.ProvidePlugin({
                //jQuery: require.resolve('jquery'),
                //paho: require.resolve('paho-mqtt'),
                //io: require.resolve('socket.io-client')
            }),
            new webpack.SourceMapDevToolPlugin({
                filename: '[file].map[query]',
            }),
        ]
    },

];